<!DOCTYPE html manifest="example.appcache">

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link href='http://fonts.googleapis.com/css?family=Coming+Soon' rel='stylesheet' type='text/css'>
    <link href="res/style.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="lib/hammer.min.js"></script>
    <script type="text/javascript" src="lib/pixi.min.js"></script>
    <script type="text/javascript" src="lib/howler.min.js"></script>
    <!--    <link rel="manifest" href="manifest.json">-->

    <link rel="icon" type="image/png" href="res/common/mediumResIcon.png"/>
    <link rel="icon" sizes="192x192" href="res/common/highResIcon.png">
    <link rel="icon" sizes="128x128" href="res/common/mediumResIcon.png">
    <link rel="apple-touch-icon" sizes="128x128" href="res/common/mediumResIcon.png">
    <link rel="apple-touch-icon-precomposed" sizes="128x128" href="res/common/mediumResIcon.png">


    <script type="text/javascript" src="src/MrGrizzy.js"></script>
    <!--        <script type="text/javascript" src="src/MrGrizzy.js --><!--"></script>-->

    <title>MrGrizzy</title>
    <meta name="viewport"
          content="user-scalable=yes, width=device-height, initial-scale=0.5, maximum-scale=1, user-scalable=no">


    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"/>

    <meta name="msapplication-tap-highlight" content="no"/>

</head>
<body id="body">
<div id="accessPanel">
    <form id="accessForm">
        <div id="topFlag"></div>
        <h1>Log in</h1>
        <input type="email" placeholder="Email" id="email"/>
        <input type="password" placeholder="Your password" id="password"/>
        <input type="checkbox" value="Save for the next login"/>
        <label>Save for the next login</label>
        <input type="submit" value="Log in" class="button" name="sender" id="sender"/>
        <button id="cancel" class="button">Not now</button>

        <label id="error"></label>
    </form>
</div>

<div id="container">



</div>
</body>

</html>