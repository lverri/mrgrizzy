<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>3</int>
        <key>texturePackerVersion</key>
        <string>3.4.0</string>
        <key>fileName</key>
        <string>C:/Users/Lorenzo/Desktop/projects/MrGrizzysHat/game/res/960/game/SpriteSheet/character/character.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>premultiplyAlpha</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>json</string>
        <key>textureFileName</key>
        <filename>character.png</filename>
        <key>flipPVR</key>
        <false/>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>2</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>0</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>2</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>reduceBorderArtifacts</key>
        <false/>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">Basic</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">AnySize</enum>
            <key>forceSquared</key>
            <false/>
            <key>forceWordAligned</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>data</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>character.json</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>cleanTransparentPixels</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>innerPadding</key>
            <uint>0</uint>
            <key>extrude</key>
            <uint>0</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">None</enum>
            <key>heuristicMask</key>
            <false/>
            <key>pivotPoint</key>
            <enum type="SpriteSettings::PivotPoint">Center</enum>
        </struct>
        <key>fileList</key>
        <array>
            <filename>../../original/character/sprite_0020_Sfondo-copia.png</filename>
            <filename>../../original/character/sprite_0021_Sfondo-copia.png</filename>
            <filename>../../original/character/sprite_0022_Sfondo-copia.png</filename>
            <filename>../../original/character/sprite_0023_Sfondo-copia.png</filename>
            <filename>../../original/character/sprite_0024_Sfondo-copia.png</filename>
            <filename>../../original/character/sprite_0025_Sfondo-copia.png</filename>
            <filename>../../original/character/sprite_0026_Sfondo-copia.png</filename>
            <filename>../../original/character/sprite_0027_Sfondo-copia.png</filename>
            <filename>../../original/character/sprite_0028_Sfondo-copia.png</filename>
            <filename>../../original/character/sprite_0000_Sfondo-copia.png</filename>
            <filename>../../original/character/sprite_0001_Sfondo-copia.png</filename>
            <filename>../../original/character/sprite_0002_Sfondo-copia.png</filename>
            <filename>../../original/character/sprite_0003_Sfondo-copia.png</filename>
            <filename>../../original/character/sprite_0004_Sfondo-copia.png</filename>
            <filename>../../original/character/sprite_0005_Sfondo-copia.png</filename>
            <filename>../../original/character/sprite_0006_Sfondo-copia.png</filename>
            <filename>../../original/character/sprite_0007_Sfondo-copia.png</filename>
            <filename>../../original/character/sprite_0008_Sfondo-copia.png</filename>
            <filename>../../original/character/sprite_0009_Sfondo-copia.png</filename>
            <filename>../../original/character/sprite_0010_Sfondo-copia.png</filename>
            <filename>../../original/character/sprite_0011_Sfondo-copia.png</filename>
            <filename>../../original/character/sprite_0012_Sfondo-copia.png</filename>
            <filename>../../original/character/sprite_0013_Sfondo-copia.png</filename>
            <filename>../../original/character/sprite_0014_Sfondo-copia.png</filename>
            <filename>../../original/character/sprite_0015_Sfondo-copia.png</filename>
            <filename>../../original/character/sprite_0016_Sfondo-copia.png</filename>
            <filename>../../original/character/sprite_0017_Sfondo-copia.png</filename>
            <filename>../../original/character/sprite_0018_Sfondo-copia.png</filename>
            <filename>../../original/character/sprite_0019_Sfondo-copia.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
    </struct>
</data>
