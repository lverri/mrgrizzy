/**
 * Created by Lorenzo on 05/06/14.
 */

/**
 * States
 * @constructor
 */
var dirRes;
var dirAud;
var canvas;
var H;
var animationLength = 400;


function IntroState() {
    var step = 5000;
    var subStateCurrent = -1;
    var banner;
    var subState = [
        ['intro', 'banner01.gif']
    ];
    var timer;

    this.update = function () {

    }

    this.destroy = function () {

    }

    this.end = function () {
        // load Game
        Sys.level.loadGame();

        clearInterval(timer);
        Sys.stage.removeChild(banner);
        //H.off('tap press', Sys.stateObject.callback);
        // switch to menu
        Sys.setState('menu');
        // initialize the state
        Sys.stateObject.load();
    };

    this.changeSubState = function () {
        //console.log('change sub state');
        subStateCurrent++;
        if (subStateCurrent >= subState.length) {
            this.end();
        } else {
            this.loadState();
        }
    };

    this.loadState = function () {
        var texture = PIXI.Texture.fromImage(dirRes + subState[subStateCurrent][1]);
        banner = new PIXI.Sprite(texture);
        banner.anchor.x = 0, banner.anchor.x = 0;
        banner.position.x = 0, banner.position.y = 0;
        Sys.stage.addChild(banner);
    };


    this.init = function () {


        Sys.iMusic = Sys.loadState('Music');
        Sys.iFxEffects = Sys.loadState('FX Effects');
        dirRes = 'res/' + Sys.sWidth + '/intro/';
        //H.on('tap press', Sys.stateObject.callback);
        Sys.stateObject.changeSubState();


        var sound = new Howl({
            urls: [dirAud + 'music/menu_intro.mp3', dirAud + 'music/menu_intro.mp3'],
            autoplay: true,
            volume: (Sys.iMusic * 0.1),
            onend: function () {

                try {
                    Sys.stateObject.changeSubState();
                    sound.unload();
                } catch (e) {

                }

            }
        });
        sound.play();

    };
}

function MenuState() {

    var assetsToLoader;
    var loader;

    var paused = false;
    var init_animation = true;

    var buttonsToLoad = ['extra_btn.png', 'play_btn.png', 'options_btn.png'];
    var decorationsToLoad = [];

    var buttons = [];
    var decorations = [];
    var bg01;
    var bg02;
    var logo;
    var flag;

    //TODO implement a loader
    this.load = function () {
        Sys.sLoaded = false;
        dirRes = 'res/' + Sys.sWidth + '/menu/';
        assetsToLoader = [(dirRes + 'SpriteSheet/MenuSpriteSheet.json')];
        loader = new PIXI.AssetLoader(assetsToLoader);
        loader.onComplete = Sys.stateObject.init;
        loader.load();
    }


    this.update = function () {

        if (Sys.sLoaded) {
            bg01.position.x--;
            bg02.position.x--;
            if (bg01.position.x <= 0 - Sys.sWidth) bg01.position.x = Sys.sWidth;
            if (bg02.position.x <= 0 - Sys.sWidth) bg02.position.x = Sys.sWidth;

            if (init_animation) {
                if (flag.position.y > Sys.sHeight / 25) {
                    init_animation = false;
                }
                flag.position.y += 10;
            }
        }

    }

    this.pause = function () {
        paused = true;

    }


    this.resume = function () {
        paused = false;

    }

    this.destroy = function () {
    }

    this.init = function () {
        Sys.sLoaded = true;


        Sys.audio.returnAudio('mainTheme', 'music').loop(true);
        Sys.audio.returnAudio('mainTheme', 'music').volume(Sys.iMusic * 0.1);
        Sys.audio.returnAudio('mainTheme', 'music').play();
        //loads decoration
        for (var i = 0; i < decorationsToLoad.length; i++) {
            var texture = PIXI.Texture.fromImage(dirRes + decorationsToLoad[i]);
            var decoration = new PIXI.Sprite(texture);
            decorations.push(decoration);
        }


        var bgText = PIXI.Texture.fromFrame('bg.gif');
        var logoTxt = PIXI.Texture.fromFrame('logo.png');
        var flagTxt = PIXI.Texture.fromFrame('title.png');
        bg01 = new PIXI.Sprite(bgText);
        bg02 = new PIXI.Sprite(bgText);
        logo = new PIXI.Sprite(logoTxt);
        flag = new PIXI.Sprite(flagTxt);
        logo.position.x = Sys.sWidth / 2 - (logo.width / 1.7);
        logo.position.y = Sys.sHeight / 3.5;
        bg01.position.x = 0;
        bg02.position.x = Sys.sWidth;
        flag.position.x = Sys.sWidth / 2 - (flag.width / 2);
        flag.position.y = 0 - flag.height;

        decorations.push(bg01, bg02, logo, flag);

        //loads moving assets
        for (var z = 0; z < decorations.length; z++) {
            Sys.stage.addChild(decorations[z]);
        }


        var posX;
        //loads buttons
        for (var i = 0; i < buttonsToLoad.length; i++) {
            var texture = PIXI.Texture.fromFrame(buttonsToLoad[i]);
            var button = new PIXI.Sprite(texture);

            button.position.y = Sys.sHeight / 1.2;
            button.interactive = true;

            switch (buttonsToLoad[i]) {
                case 'extra_btn.png':
                    button.click = button.tap = function () {
                        if (!paused) {
                            Sys.pauseState();
                            Sys.sLoaded = false;
                            Sys.setState('extras');
                            Sys.stateObject.load();
                            Sys.audio.returnAudio('buttonPress', 'fx').play();
                        }
                    }
                    posX = (Sys.sWidth / 2) + button.width * 0.8;
                    break;
                case 'play_btn.png':
                    button.click = button.tap = function () {
                        //Bring up the selection
                        if (!paused) {
                            Sys.pauseState();
                            Sys.setState('level_selection');
                            Sys.stateObject.load();
                            Sys.audio.returnAudio('buttonPress', 'fx').play();
                        }
                    }
                    posX = (Sys.sWidth / 2) - (button.width / 2);
                    break;
                case 'options_btn.png':
                    button.click = button.tap = function () {
                        //Bring up the options
                        if (!paused) {

                            /**
                             * Dirty fix. Bug in the library. TODO: to be removed
                             */
                            Sys.audio.returnAudio('mainTheme', 'music').loop(false);
                            Sys.audio.returnAudio('mainTheme', 'music').stop();
                            Sys.audio.returnAudio('mainTheme', 'music').loop(false);
                            Sys.audio.returnAudio('mainTheme', 'music').stop();

                            Sys.pauseState();
                            Sys.sLoaded = false;
                            Sys.setState('options');
                            Sys.stateObject.load();
                            Sys.audio.returnAudio('buttonPress', 'fx').play();
                        }
                    }
                    posX = (Sys.sWidth / 2) - button.width * 1.8;
                    break;
            }
            button.position.x = posX;

            Sys.stage.addChild(button);
            buttons.push(button);


        }
        var facebook = new PIXI.Sprite(PIXI.Texture.fromFrame('fb.png'));
        facebook.interactive = true;
        facebook.tap = facebook.click = function () {
            var win = window.open('https://www.facebook.com/pages/MrGrizzy/264096807047566?fref=ts', '_blank');
            win.focus();
        }


        var v = new PIXI.Text('MrGrizzy Alpha v0.1a', {font: '14px Coming Soon', fill: 'black'});
        v.position.x = 40;
        v.position.y = 30;

        Sys.stage.addChild(v);
        Sys.stage.addChild(facebook);
        facebook.position.y = 0 + facebook.width / 4;
        facebook.position.x = Sys.sWidth - facebook.width * 1.2;

    }

    this.end = function () {

    }


    this.callback = function () {
    }


}


function OptionsState() {

    var assetsToLoader;
    var loader;


    var backgrounds = [];
    var backgrounds_length = 0;
    var objects = [];
    var mainArrows = [];
    var optionArrows = [];
    var title;

    var paused = false;
    var init_animation = true;
    var end_animation = false;
    var move_animation = false;
    var direction = true;

    var options = ['Difficulty', 'Music', 'FX Effects'];
    var currentOption = 0;

    var valueText;
    var titleText;
    var sampleSound;

    function updateSettings(bIncrease) {
        console.log(options[currentOption]);
        switch (options[currentOption]) {
            case 'Difficulty':

                if (bIncrease) {
                    if (Sys.iDifficulty < 4) {
                        Sys.iDifficulty++;
                    } else {
                        Sys.iDifficulty = 1;
                    }
                } else if (bIncrease === false) {
                    if (Sys.iDifficulty > 1) {
                        Sys.iDifficulty--;
                    } else {
                        Sys.iDifficulty = 4;
                    }
                }


                valueText.setText(getDifficultyName());
                break;
            case 'FX Effects':
                if (bIncrease) {
                    if (Sys.iFxEffects < 10) {
                        Sys.iFxEffects++;
                    } else {
                        Sys.iFxEffects = 0;
                    }
                } else if (bIncrease === false) {
                    if (Sys.iFxEffects > 0) {
                        Sys.iFxEffects--;
                    } else {
                        Sys.iFxEffects = 10;
                    }
                }

                var sample = Sys.audio.returnAudio('buttonPress', 'fx').stop();
                Sys.audio.setVolume('fx');
                sample.play();

                valueText.setText(Sys.iFxEffects + '/10');
                break;
            case 'Music':
                if (bIncrease) {
                    if (Sys.iMusic < 10) {
                        Sys.iMusic++;
                    } else {
                        Sys.iMusic = 0;
                    }
                } else if (bIncrease === false) {
                    if (Sys.iMusic > 0) {
                        Sys.iMusic--;
                    } else {
                        Sys.iMusic = 10;
                    }

                }

                try {

                    sampleSound.stop();
                } catch (e) {
                    console.log(e);
                }

                sampleSound = new Howl({
                    urls: [dirAud + 'music/theMountain_intro.mp3', dirAud + 'music/theMountain_intro.ogg'],
                    loop: false,
                    volume: (Sys.iMusic * 0.1)
                }).play();

                valueText.setText(Sys.iMusic + '/10');

                break;
        }
    }

    function getDifficultyName() {
        // TODO add the option to change difficulty
        if (Sys.iDifficulty === 1) return 'medium';
        if (Sys.iDifficulty === 2) return 'medium';
        if (Sys.iDifficulty === 3) return 'medium';
        if (Sys.iDifficulty === 4) return 'medium';
        return '';
    }

    function saveSettingValue() {
        var value;
        if (options[currentOption] === 'Difficulty') {
            value = Sys.iDifficulty;
        } else if (options[currentOption] === 'FX Effects') {
            value = Sys.iFxEffects;
        } else if (options[currentOption] === 'Music') {
            value = Sys.iMusic;
        }
        Sys.saveState(options[currentOption], value);
    }

    function nextOption() {
        direction = true;
        mainArrows[1].visible = true;

        //TODO this could be saved when switching to new tab
        saveSettingValue();

        if (currentOption + 1 < backgrounds_length) {
            currentOption++;
        }
        if (currentOption + 1 == backgrounds_length) {
            mainArrows[2].visible = false;
        }
        move_animation = true;
        titleText.setText(options[currentOption]);
        updateSettings();
    }

    function prevOption() {

        direction = false;
        mainArrows[2].visible = true;
        saveSettingValue();

        if (currentOption - 1 > -1) {
            currentOption--;
        }
        if (currentOption - 1 === -1) {
            // hide the next arrow
            mainArrows[1].visible = false;
        }

        titleText.setText(options[currentOption]);
        updateSettings();
        move_animation = true;
    }


    this.update = function () {
        if (Sys.sLoaded) {
            /**
             * Move in animation
             */
            if (init_animation) {
                if (backgrounds[0].position.x > 0) {
                    for (var i = 0; i < backgrounds_length; i++) {
                        backgrounds[i].position.x -= 60
                    }
                } else {
                    init_animation = false;
                    updateSettings();
                    titleText.setStyle({font: '50px Coming Soon', fill: 'black'});
                }
            }

            /**
             * Move out animation
             */
            if (end_animation) {
                if (backgrounds[0].position.x < Sys.sWidth) {
                    for (var i = 0; i < backgrounds_length; i++) {
                        backgrounds[i].position.x += 60;
                    }
                } else {
                    end_animation = false;
                }
            }

            /**
             * Animation While Moving
             */
            var i = 0;

            if (move_animation) {
                if (direction) {
                    if (backgrounds[0].position.x > ((-1) * (Sys.sWidth * currentOption))) {
                        for (i = 0; i < backgrounds_length; i++) {
                            backgrounds[i].position.x -= 60;
                            objects[i].position.x -= 60;
                        }
                    } else {
                        move_animation = false;
                    }
                } else {
                    if (backgrounds[0].position.x < ((-1) * (Sys.sWidth * currentOption))) {
                        for (i = 0; i < backgrounds_length; i++) {
                            backgrounds[i].position.x += 60;
                            objects[i].position.x += 60;
                        }
                    } else {
                        move_animation = false;
                    }
                }
            }


        }
    }


    this.load = function () {
        dirRes = 'res/' + Sys.sWidth + '/menu/';
        assetsToLoader = [(dirRes + 'SpriteSheet/OptionsSpriteSheet.json')];
        loader = new PIXI.AssetLoader(assetsToLoader);
        loader.onComplete = Sys.stateObject.init;
        titleText = new PIXI.Text(options[currentOption]);
        valueText = new PIXI.Text(getDifficultyName(), {font: '35px Coming Soon', fill: 'black'});
        loader.load();
    }

    this.init = function () {
        var texture = PIXI.Texture.fromFrame('options_bg.gif');
        var posX = Sys.sWidth;
        for (var i = 0; i < 3; i++) {
            var sprite = new PIXI.Sprite(texture);
            sprite.position.x = posX;
            sprite.position.y = 0;
            backgrounds.push(sprite);
            Sys.stage.addChild(backgrounds[i]);
            posX += Sys.sWidth;
        }
        backgrounds_length = backgrounds.length;


        var objectsNames = ['notebook.png', 'audiocassette.png', 'plate.png'];

        posX = Sys.sWidth / 2;

        for (var i = 0; i < objectsNames.length; i++) {
            var sprite = new PIXI.Sprite(PIXI.Texture.fromFrame(objectsNames[i]));
            sprite.position.y = (Sys.sHeight / 2) - (sprite.height / 1.7);
            sprite.position.x = (posX - (sprite.width / 2));
            posX += Sys.sWidth;
            objects.push(sprite);
            Sys.stage.addChild(sprite);
        }


        for (var i = 0; i < 2; i++) {
            var sprite = new PIXI.Sprite(PIXI.Texture.fromFrame('options_arrow.png'));
            sprite.position.x = ((Sys.sWidth / 2) - (sprite.width * 4));
            sprite.interactive = true;
            if (i === 1) {
                sprite.click = sprite.tap = function () {
                    //increase current setting
                    updateSettings(true);
                }
                sprite.position.x = Sys.sWidth / 2 + sprite.width * 4;
                sprite.scale.x = -1;
            } else {
                sprite.click = sprite.tap = function () {
                    //decrease current setting

                    updateSettings(false);
                }
            }

            sprite.position.y = Sys.sHeight - sprite.height * 1.5;
            optionArrows.push(sprite);
            Sys.stage.addChild(sprite);
        }

        // THIS SHOULD NOT CHANGE
        const arrows = ['arrow_back.png', 'arrow_prev.gif', 'arrow_next.gif'];

        for (var i = 0; i < arrows.length; i++) {
            var sprite = new PIXI.Sprite(PIXI.Texture.fromFrame(arrows[i]));
            sprite.interactive = true;
            switch (arrows[i]) {
                case 'arrow_back.png':
                    sprite.position.x = 0 + (sprite.width / 2);
                    sprite.position.y = sprite.height / 2;
                    sprite.click = sprite.tap = function () {
                        saveSettingValue();
                        Sys.audio.returnAudio('buttonPress', 'fx').play();
                        Sys.stateObject.end();
                    }
                    break;
                case 'arrow_prev.gif':
                    sprite.visible = false;
                    sprite.position.x = 0 + (sprite.width / 2);
                    sprite.position.y = (Sys.sHeight / 2) - (sprite.height / 2);
                    sprite.click = sprite.tap = function () {
                        Sys.audio.returnAudio('buttonPress', 'fx').play();
                        prevOption();
                    }
                    break;
                case 'arrow_next.gif':
                    sprite.position.x = Sys.sWidth - (sprite.width + (sprite.width / 2));
                    sprite.position.y = (Sys.sHeight / 2) - (sprite.height / 2);
                    sprite.click = sprite.tap = function () {
                        Sys.audio.returnAudio('buttonPress', 'fx').play();
                        nextOption();
                    }
                    break;
            }
            Sys.stage.addChild(sprite);
            mainArrows.push(sprite);
        }


//        TODO continue
        valueText.position.x = Sys.sWidth / 2 - valueText.width / 2.20;
        valueText.position.y = Sys.sHeight - sprite.height;
        Sys.stage.addChild(valueText);
        valueText.setStyle({font: '40px Coming Soon', fill: 'black'});

        titleText.position.x = Sys.sWidth / 2 - titleText.width / 1.3;
        titleText.position.y = Sys.sHeight - sprite.height * 2;
        Sys.stage.addChild(titleText);
        titleText.setStyle({font: '50px Coming Soon', fill: 'black'});

        title = new PIXI.Sprite(PIXI.Texture.fromFrame('options_title.png'));
        Sys.stage.addChild(title);
        title.position.x = Sys.sWidth / 2 - title.width / 2;
        title.position.y = title.height / 1.8;
        title.rotation = -0.05;

        currentOption = 0;
        Sys.sLoaded = true;

        H.on('swiperight swipeleft', function (e) {
            Sys.stateObject.callback(e);
        });

    }

    this.destroy = function () {
        for (var i = 0; i < backgrounds_length; i++) {
            Sys.stage.removeChild(backgrounds[i]);
        }
        Sys.audio.returnAudio('mainTheme', 'music').loop(true);
        Sys.audio.returnAudio('mainTheme', 'music').volume(Sys.iMusic * 0.1);
        Sys.audio.returnAudio('mainTheme', 'music').play();

        H.off('swiperight swipeleft', function (e) {
            Sys.stateObject.callback(e);
        });

    }

    this.callback = function (evt) {
        Sys.audio.returnAudio('buttonPress', 'fx').play();
        switch (evt.type) {
            case 'swipeleft':
                nextOption();
                console.log('SpanLeft');
                break;
            case 'swiperight':
                prevOption();
                console.log('SpanRight');
                break;
        }

    }

    this.end = function () {
        for (var i = 0; i < mainArrows.length; i++) {
            Sys.stage.removeChild(mainArrows[i]);
        }

        for (var i = 0; i < optionArrows.length; i++) {
            Sys.stage.removeChild(optionArrows[i]);
        }

        for (var i = 0; i < objects.length; i++) {
            Sys.stage.removeChild(objects[i]);
        }

        Sys.stage.removeChild(valueText);
        Sys.stage.removeChild(titleText);
        Sys.stage.removeChild(title);


        end_animation = true;
        setTimeout(function () {
            Sys.stateObject.destroy();
            Sys.resumeState();
        }, (animationLength * (currentOption + 1)));
    }

}

function LevelSelectionState() {

    var assetsToLoader;
    var loader;

    var iMove = 1;
    var iDistance = 0;

    var bDirArrow = true;
    var bInitAnimation;
    var bEndAnimation;

    var mapSprite;
    var backBtn;
    var nextLevelBtn;
    var prevLevelBtn;
    var text;


    var worldsNames;
    /**
     * Selected level is the level going to be played by the user.
     * @type {number}
     */
    var selectionWorld = 0;
    var selectedWorld = 1;

    /**
     * Sprites of the levels
     */
    var levelsBlocks;

    /**
     * Object level loaded by json file
     */
    var subLevelObj;
    var title;
    /**
     *  Number of levels played -> they are now unlocked
     *  It is saved in the local storage as "Progress".
     */
    var savedGame = {
        "worlds": {
            "The_Mountain": 1,
            "The_Little_Hill": 1,
            "The_Seaside": 1
        }
    };

    this.update = function () {
        if (Sys.sLoaded) {
            /**
             * Animation start
             */
            if (bInitAnimation) {
                mapSprite.position.x -= 60;
                if (mapSprite.position.x <= 0) {
                    bInitAnimation = false;
                    Sys.stage.addChild(backBtn);
                    Sys.stage.addChild(nextLevelBtn);
                    Sys.stage.addChild(prevLevelBtn);
                    Sys.stage.addChild(text);
                }
            }

            /**
             * Removes the maps
             */
            if (bEndAnimation) {
                mapSprite.position.x += 60;
                if (mapSprite.position.x >= Sys.sWidth)
                    bEndAnimation = false;
            }


            if (bDirArrow) {
                nextLevelBtn.position.x += iMove;
                prevLevelBtn.position.x -= iMove;
            } else {
                nextLevelBtn.position.x -= iMove;
                prevLevelBtn.position.x += iMove;
            }

            if (iDistance === 30) {
                iDistance = 0;
                bDirArrow = (bDirArrow) ? false : true;
            }
            iDistance++;
        }
    }


    function loadingCallback(e) {
        subLevelObj = Sys.level.process(e);
    }


    this.load = function () {
        savedGame = Sys.level.process(Sys.level.loadGame());

        Sys.sLoaded = false;
        dirRes = 'res/' + Sys.sWidth + '/menu/';
        loadingCallback(Sys.ajax.ajaxSync('', 'res/levels/levels.json'));
        //Sys.ajax.newRequest('', 'res/levels/levels.json', loadingCallback);
        assetsToLoader = [dirRes + 'SpriteSheet/LevelSelectionSpriteSheet.json'];
        loader = new PIXI.AssetLoader(assetsToLoader);
        text = new PIXI.Text('', {font: '50px Coming Soon', fill: 'black'});
        loader.onComplete = Sys.stateObject.init;
        loader.load();
    }


    function _selectLevelAndAdvance(e, worldName) {
        Sys.level.iLevel = e.target.levelID; //TODO insert the right level
        Sys.level.sWorld = worldName;
        Sys.stateObject.end('forward');

        /**
         * Dirty fix. Bug in the library. TODO: to be removed
         */
        Sys.audio.returnAudio('mainTheme', 'music').loop(false);
        Sys.audio.returnAudio('mainTheme', 'music').stop();
        Sys.audio.returnAudio('mainTheme', 'music').loop(false);
        Sys.audio.returnAudio('mainTheme', 'music').stop();


    }

    /*
     * It is used to change the levels block
     */
    function _changeWorld() {
        var numberOfTotalLevels = 1;
        var numberOfPlayedLevels = 1;
        var worldName = '';

        // Get the world names
        var z = 0;
        var world;
        for (world in savedGame.worlds) {
            if (z == selectionWorld) {
                numberOfPlayedLevels = savedGame.worlds[world];
                worldName = world.toString();


                break;
            }
            z++;
        }


        z = 0;
        // Get the number of total level played
        var numbLevels;
        for (numbLevels in subLevelObj.worlds) {
            if (z == selectionWorld) {
                numberOfTotalLevels = subLevelObj.worlds[numbLevels];
                break;
            }
            z++;
        }

        var centreDistance = 5.5;

        var x = Sys.sWidth / centreDistance;
        var y = Sys.sHeight / 5.3;

        var text;

        for (var i = levelsBlocks.length; i > 0; i--) {
            Sys.stage.removeChild(levelsBlocks[i - 1]);
            levelsBlocks.pop();
        }

        // Display the button for each level
        for (var z = 1; z < numberOfTotalLevels; z++) {
            var obj;
            if ((z % 4) == 1 && z != 1) {
                y += Sys.sHeight / 5;
                x = Sys.sWidth / centreDistance;
            }
            if (z <= numberOfPlayedLevels) {
                obj = new PIXI.Sprite(PIXI.Texture.fromFrame('unlocked.png'));
                text = new PIXI.Text(z.toString(), {
                    font: '50px Coming Soon',
                    fill: '#052a23'
                });

                // CENTER OF THE ELEMENT

                text.position.x = x + (obj.width / 2 - text.width / 2);
                text.position.y = y + (obj.height / 2 - text.height / 3);
                obj.interactive = true;
                // callback to advance level
                obj.levelID = z;
                obj.click = obj.tap = function (e) {
                    _selectLevelAndAdvance(e, worldName);
                }


            } else {
                obj = new PIXI.Sprite(PIXI.Texture.fromFrame('locked.png'));
            }


            obj.position.x = x;
            obj.position.y = y;

            Sys.stage.addChild(obj);

            if (text != undefined) {
                levelsBlocks.push(text);
                Sys.stage.addChild(text);
            }

            levelsBlocks.push(obj);


            x += Sys.sWidth / 6.5;
        }
    }


    this.init = function () {
        levelsBlocks = [];
        worldsNames = [];
        var level;


        for (level in subLevelObj.worlds) {
            worldsNames.push(level.toString().replace(/_/g, ' '));
            //Try to add the number of played levels to an array.
        }


        Sys.sLoaded = true;
        bInitAnimation = true;
        mapSprite = new PIXI.Sprite(PIXI.Texture.fromImage('map.gif'));
        mapSprite.anchor.x = 0;
        mapSprite.anchor.y = 0;
        mapSprite.position.x = Sys.sWidth;
        Sys.stage.addChild(mapSprite);


        title = new PIXI.Sprite(PIXI.Texture.fromImage('chooseLevel_title.png'));
        title.position.x = Sys.sWidth / 2 - title.width / 2;
        title.position.y = title.height / 1.8;
        Sys.stage.addChild(title);


        backBtn = new PIXI.Sprite(PIXI.Texture.fromImage('arrow_back.png'));
        backBtn.interactive = true;

        backBtn.click = backBtn.tap = function () {
            Sys.audio.returnAudio('buttonPress', 'fx').play();
            Sys.stateObject.end('back');

        }

        text.position.x = Sys.sWidth / 3;
        text.position.y = Sys.sHeight - (Sys.sHeight / 6);
        nextLevelBtn = new PIXI.Sprite(PIXI.Texture.fromImage('arrow_next.gif'));
        nextLevelBtn.interactive = true;
        prevLevelBtn = new PIXI.Sprite(PIXI.Texture.fromImage('arrow_prev.gif'));
        prevLevelBtn.interactive = true;
        text.setStyle({font: '50px Coming Soon', fill: 'black'});
        text.setText(worldsNames[selectionWorld]);
        text.interactive = true;
        _changeWorld();


        nextLevelBtn.click = nextLevelBtn.tap = function () {
            if (selectionWorld < worldsNames.length - 1) {
                selectionWorld++
            } else {
                selectionWorld = 0
            }
            ;
            Sys.audio.returnAudio('buttonPress', 'fx').play();
            text.setText(worldsNames[selectionWorld]);
            _changeWorld();
        }

        prevLevelBtn.click = prevLevelBtn.tap = function () {
            if (selectionWorld > 0) {
                selectionWorld--;
            } else {
                selectionWorld = worldsNames.length - 1;
            }
            ;
            Sys.audio.returnAudio('buttonPress', 'fx').play();
            text.setText(worldsNames[selectionWorld]);
            _changeWorld();
        }


        backBtn.position.x = 0 + (backBtn.width / 2);
        backBtn.position.y = backBtn.height / 2;
        prevLevelBtn.position.y = Sys.sHeight - (prevLevelBtn.height * 1.5);
        prevLevelBtn.position.x = 0 + (prevLevelBtn.width);
        nextLevelBtn.position.y = Sys.sHeight - (nextLevelBtn.height * 1.5);
        nextLevelBtn.position.x = Sys.sWidth - (nextLevelBtn.width * 2);

    }

    this.destroy = function () {

    }

    this.end = function (evt) {
        for (var i = 0; i < levelsBlocks.length; i++) {
            Sys.stage.removeChild(levelsBlocks[i]);
        }

        Sys.stage.removeChild(title);
        Sys.stage.removeChild(prevLevelBtn);
        Sys.stage.removeChild(nextLevelBtn);
        Sys.stage.removeChild(backBtn);
        Sys.stage.removeChild(text);
        bEndAnimation = true;

        /**
         * Back to menu
         */
        if (evt === 'back') {
            setTimeout(function () {
                Sys.stage.removeChild(mapSprite);
                Sys.resumeState();
            }, animationLength);

        } else {
            /**
             * Start Playing
             */
                //Sys.exitFullScreen(document);
                //    Sys.audio.returnAudio('mainTheme', 'music').stop();
            Sys.audio.returnAudio('mainTheme', 'music').stop();


            setTimeout(function () {

                Sys.stage.removeChild(mapSprite);
                Sys.setState('cinema');
                Sys.stateObject.load('intro');
                //Sys.fullScreen(video);

            }, animationLength);

        }
    }
}

function GameState() {
    var isMobile = true;

    var timeStart;
    var timeEnd;


    /**
     * Score is the score given by the quality of the collectables
     * @type {number}
     */
    var score = 0;
    /**
     * Nr of collectables gathered by the user
     * @type {number}
     */
    var collectables = 0;
    /**
     * Number of collectables to gather in the whole level
     */
    var totalCollectables;
    var specialCollectable_txt;
    var time_txt;
    var score_txt;
    var scoreToReach;

    var scoreText;
    /**
     * Potential time
     */
    var potentialTime;
    var timeInFrames = 0;
    /**
     * Loader
     */
    var loaderBg;
    var loaderState = 0;
    var loaderStates = ['graphics', 'audio', 'level', 'end'];
    /**
     * Game
     */
    var assetsToLoader;
    var loader;
    var bInitAnimation;
    /**
     * Level
     */
    var backgrounds;
    var backgroundWidth;
    var bEndAnimation;
    /**
     * Extra
     */
    var text;
    var audioSprite;
    /**
     * Pause Panels
     */
    var panel;
    var blackBg;
    var finishPanel;
    var finishPanelLose;
    /**
     * Buttons Menu
     */
    var btnContinue;
    var btnContinueEnd;
    var btnRestart;
    var btnQuit;
    /**
     * UI Buttons
     */
    var btnPause;
    /**
     * Text
     */
    var pauseText;
    var scoreText;
    /**
     * Foregrounds
     */
    var foregrounds;
    /**
     * Character
     * @type {Array}
     */
    var character = [];
    var character_crouch = [];

    var levelObject;
    var game;

    var graphics = new PIXI.Graphics();

    /**
     *
     */
    var intervalTutorial;
    var intervalBg;
    var intervalTutorialSprite;
    var tutorialText;
    /**
     * Ending
     * @type {boolean}
     */
    var bEnding = false;
    var bEndingScore = false;

    /**
     * Buttons on right and left of the screen
     */
    var runningButton = [];

    var introText;
    var introInterval;
    var introDecoration;

    function Game() {


        /**
         * Number of frames passed since the beginning of the level
         * @type {number}
         */

        this.inst;
        this._GROUNDY;
        this.groundY;


        this.character = {};
        this.characterHeight;
        this.objects = [];


        /**
         * Accelleration is increased by pressing the two buttons right and left in a continous loop
         * @type {number}
         */
        this.accelleration = 0;
        this.accellerationRythm = 0;

        /**
         *  Arrays containing the level game codes
         * @type {Array}
         */
        this.ctable = [];
        // level table
        this.ltable = [];
        // timeoutable
        this.ttable = [];

        this.ltimeout = 0;
        this.currentPosition = 0;

        this.line = 1;
        this.stepSize = 0;
        this.lastStep;
        /**
         * Initialize itself calling its own instance
         * @param inst
         */
        this.init = function (inst) {
            this.inst = inst;
            try {
                if (levelObject.Level.length != levelObject.Timeouts[0].length) throw new Sys.mrgrizzException("The level length does not match the number of inputs. Inputs:" + levelObject.Level.length + ", timeouts:" + levelObject.Timeouts[0].length);
            } catch (e) {
                alert(e.message);
            }
            this.inst.ctable = levelObject.ConversionTable;
            this.inst.ltable = levelObject.Level;
            this.inst.ttable = levelObject.Timeouts;
            scoreToReach = levelObject.MinimumScore;
            this.inst.finalPosition = levelObject.Level.length;
            this.inst._GROUNDY = (Sys.sHeight - 50);
            this.inst.finalPosition = this.inst.ltable.length;
            this.stepSize = Math.ceil(Sys.sWidth / this.inst.finalPosition);

            // Calculate potential time
            potentialTime = 0;
            for (var i = 0; i < this.inst.ttable[0].length; i++) {
                var value = parseFloat(this.inst.ttable[0][i]);
                potentialTime += parseFloat(value);
            }

            // Calculate potential nrOfObjects
            totalCollectables = 0;
            for (var z = 0; z < this.inst.ltable.length; z++) {
                for (i = 0; i < this.inst.ltable[z].length; i++) {
                    var objectID = this.inst.ltable[z][i];
                    if (objectID < 5 && objectID != 0) {
                        totalCollectables++;
                    }
                }
            }


        }


        this.update = function () {
            // increase the timer of 1
            timeInFrames++;

            var positionBearY = this.inst.character.position.y;
            var positionBearX = this.inst.character.position.x;

            var positionYPlusHeight = positionBearY + 147;
            var positionXPlusWidth = positionBearX + this.inst.character.width;

            var b_leftToCheckHit = true;
            var b_leftToRemove = true;

            this.inst.lastStep = (this.inst.currentPosition + 1) * this.inst.stepSize;


            var _offset = 20;
            var _offset2 = 10;

            for (var i = 0, len = this.inst.objects.length; i < len; i++) {
                /** MOVE THE OBJECTS **/
                this.inst.objects[i].sprite.position.x -= this.inst.physics.velocityX << 1;
                /**
                 * COLLISION DETECTION
                 */
                if (b_leftToCheckHit) {
                    if (this.inst.objects[i].type > 1) {
                        if (this.inst.objects[i].sprite.position.x <= positionXPlusWidth - (_offset2 + 5)) {
                            if (this.inst.objects[i].sprite.position.y < positionYPlusHeight - _offset2 && this.inst.objects[i].sprite.position.y + this.inst.objects[i].sprite.height > positionBearY && this.inst.objects[i].sprite.position.x + this.inst.objects[i].sprite.width >= positionBearX + _offset + 5) {
                                /** HIT DETECTION FOR BONUS **/
                                if (this.inst.objects[i].type === 2) {
                                    score += this.inst.objects[i].scoreValue;
                                    collectables++;
                                    scoreText.setText("Score: " + score.toString());
                                    scoreText.setStyle({font: '48px Coming Soon', fill: "black"});
                                    this.inst.objects[i].type = 0;
                                    audioSprite.play('munching');
                                    Sys.stage.removeChild(this.inst.objects[i].sprite);
                                } else {
                                    /** HIT DETECTION **/
                                        // if (score - this.inst.objects[i].scoreValue > 0) {
                                    score -= this.inst.objects[i].scoreValue;
                                    if (score < 0) score = 0;
                                    // }
                                    scoreText.setStyle({font: '48px Coming Soon', fill: 'black'});
                                    scoreText.setText("Score: " + score.toString());
                                    this.inst.objects[i].type = 0;
                                    audioSprite.play('hit0' + Math.floor((Math.random() * 2) + 1));
                                }
                            }
                        } else {
                            b_leftToCheckHit = false;
                        }
                    }
                }
                /** GARBAGE COLLECTION
                 * - Checks the first ones. If first column is not out yet,
                 * id does not make sense to keep checking**/
                if (b_leftToRemove) {
                    if ((this.inst.objects[i].sprite.position.x + this.inst.objects[i].sprite.width) < 0) {
                        if (this.inst.objects[i].type != 0) {
                            Sys.stage.removeChild(this.inst.objects[i].sprite);
                        }
                        this.inst.objects.shift();
                        i--;
                        len--;
                    } else {
                        b_leftToRemove = false;
                    }
                }
            }
            if (bEnding) {
                _gameOver();
            }


            /**
             * Sync objects with time and gameplay
             * (the faster the player runs the faster the objects are going to be spawned
             */
            if (this.inst.physics.velocityX === 8) {
                this.inst.ltimeout += 2;
            } else if (this.inst.physics.velocityX === 12) {
                this.inst.ltimeout += 3;
            } else {
                this.inst.ltimeout++;
            }


            /** FUNCTION SPAWNING **/
            if (this.inst.ltimeout >= this.inst.ttable[0][this.inst.currentPosition] * Sys.framesPerSecond) {
                // ADD OBJECTS

                for (i = 0; i < this.inst.ltable[this.inst.currentPosition].length; i++) {
                    var objectID = this.inst.ltable[this.inst.currentPosition][i];
                    if (objectID === 20) {
                        _gameOver();
                        break;
                    }
                    else {
                        if (objectID != 0) {
                            // CHECKS WHAT TYPE OF OBJECT THAT IS;
                            var sprite = new PIXI.Sprite(PIXI.Texture.fromImage('landscape_' + this.inst.ctable[objectID - 1][1] + '.png'))
                            sprite.position.x = Sys.sWidth;
                            sprite.position.y = Sys.sHeight - (((i + 1) * 60) + sprite.height);
                            if (objectID == 7 || objectID == 12) {
                                sprite.position.y = Sys.sHeight - (((i + 1) * 60) + sprite.height / 1.5);
                            }

                            /** CREATE OBJECT **/
                            /** PLATFORM ARE THE MOST COMMON OBJECTS **/
                            if (objectID > 12) {
                                /** PLATFORM **/
                                sprite = {type: 1, sprite: sprite}
                            } else if (objectID < 5) {
                                /** BONUS **/
                                sprite = {
                                    type: 2,
                                    value: objectID * 5,
                                    sprite: sprite,
                                    scoreValue: this.inst.ctable[objectID - 1][2]
                                };
                            } else if (objectID < 13) {
                                /** OBSTACLE **/
                                sprite = {
                                    type: 3,
                                    value: Math.abs(objectID / 2),
                                    sprite: sprite,
                                    scoreValue: this.inst.ctable[objectID - 1][2]
                                };
                            }
                            this.inst.objects.push(sprite);
                            Sys.stage.addChild(sprite.sprite);


                            Sys.stage.swapChildren(sprite.sprite, game.character);

                        }
                    }
                }
                this.inst.ltimeout = 0;
                this.inst.currentPosition++;
            }

            // SHOWS THE GROUND LEVEL
            //graphics.clear();

            var groundY = _determineGroundY(positionBearX, positionYPlusHeight, positionXPlusWidth, this.inst.objects, this.inst._GROUNDY);


            runningButton[0].alpha = 0.3;
            runningButton[1].alpha = 0.3;


            // CHECKS DISTANCE FROM THE GROUND
            if (((positionYPlusHeight + this.inst.physics.velocityY) < groundY)) {
                positionBearY = positionBearY + this.inst.physics.velocityY;
                this.inst.physics.velocityY += this.inst.physics.GRAVITY;
            } else {
                this.inst.character.play();
                this.inst.physics.velocityY = 0;
                this.inst.actions.isJumping = false;
            }


            /**
             * PLAY ANIMATION CHARACTER
             */
            if (this.inst.actions.isJumping === false) {
                this.inst.character.play();
            }

            if (this.inst.buttonPressed.isDecelleratingButtonPressed === true) {
                this.inst.accellerationRythm = 0;
            }

            if (this.inst.buttonPressed.isAccelleratingButtonPressed === true && this.inst.accellerationRythm < Sys.framesPerSecond && this.inst.buttonPressed.isDecelleratingButtonPressed === true) {
                if (this.inst.accelleration < 24) {
                    // ACCELLERATE


                    // TODO Improve the performance of this part by caching the user agent type (desktop or mobile)
                    if (isMobile) {
                        this.inst.accelleration += 2;
                    } else {
                        this.inst.accelleration += 2;
                    }


                    if (this.inst.accelleration > 9) {
                        //MAKE A LIST OF OPERATION
                        if (this.inst.accelleration < 13) {
                            this.inst.physics.velocityX = 4;
                        }
                        if (this.inst.accelleration < 20) {
                            this.inst.physics.velocityX = 8;
                        }
                        if (this.inst.accelleration > 19) {

                            this.inst.physics.velocityX = 12;
                        }
                    }

                }

                //RESET THE BUTTONS
                this.inst.buttonPressed.isAccelleratingButtonPressed,
                    this.inst.buttonPressed.isDecelleratingButtonPressed = false;
            }

            // DECELLERATOR PART
            if (this.inst.accellerationRythm > Sys.framesPerSecond) {
                if ((this.inst.physics.velocityX / 4  ) > 1) {

                    if (this.inst.accelleration < 10) {
                        this.inst.physics.velocityX = 4;
                    }
                    if (this.inst.accelleration >= 10) {
                        this.inst.physics.velocityX = 8;
                    }
                    if (this.inst.accelleration > 20) {
                        this.inst.physics.velocityX = 12;
                    }

                    this.inst.physics.velocityX = this.inst.physics.velocityX / 2;
                }

                if ((this.inst.accelleration - 2) >= 0) {
                    this.inst.accelleration -= 2;
                }

                this.inst.accellerationRythm = 0;
            }


            /**
             * JUMPING
             */
            if (this.inst.actions.isJumping === false && this.inst.buttonPressed.isJumpButtonPressed === true) {

                if (this.inst.actions.isCrouching === true) {
                    //TODO add sound
                    this.inst.actions.isCrouching = false;
                    this.inst.character.position.y -= 60;
                    this.inst.character.textures = character;
                }

                this.inst.physics.velocityY = -30;

                if (this.inst.physics.velocityX >= 8) {
                    this.inst.physics.velocityY = -40;
                }


                this.inst.actions.isJumping = true;

                audioSprite.play('jump0' + Math.floor((Math.random() * 2) + 1));

                this.inst.buttonPressed.isJumpButtonPressed = false;
                // jumping
                this.inst.character.gotoAndStop(28);
            }
            /**
             * CROUCHING - getting off platform
             */
            if (this.inst.buttonPressed.isCrouchButtonPressed === true && this.inst.actions.isJumping === false && (positionYPlusHeight < Sys.sHeight - 50)) {
//                this.inst.actions.isCrouching = true;
                positionBearY += 20;
                audioSprite.play('downTrail');
                //this.inst.character.textures = character_crouch;
                this.inst.buttonPressed.isCrouchButtonPressed = false;
            }
            /**
             * Jumping ENDS
             */
            this.inst.character.position.y = positionBearY;
            this.inst.accellerationRythm++;

            /**
             * Monitor in level
             */
            if (this.inst.currentPosition <= this.inst.finalPosition) {
                if (this.inst.line <= this.inst.lastStep) {
                    this.inst.line += this.inst.physics.velocityX / 4;
                    graphics.clear();
                    graphics.lineStyle(5, 0xFF7885, 1);
                    graphics.moveTo(0, Sys.sHeight);
                    graphics.lineTo(this.inst.line, Sys.sHeight);
                }
            }


            /**
             * ENDING
             */
            if (bEnding) {
                _gameOver();
            }

        }

        /**
         * Returns the closest instance of the underlying ground
         * @param cPosX
         * @param cPosYPlushHeight
         * @param cPosXPlusWidth
         * @param objects
         * @param groundY
         * @param _GROUNDYCONS
         * @returns {*}
         * @private
         */
        function _determineGroundY(cPosX, cPosYPlushHeight, cPosXPlusWidth, objects, _GROUNDYCONS) {
            var groundY = _GROUNDYCONS;
            for (var i = 0, len = objects.length; i < len; i++) {

                if (objects[i].type === 1) {

                    // if the object x is < than bearx and object x > bearx
                    if (objects[i].sprite.position.x < cPosXPlusWidth && objects[i].sprite.position.x + objects[i].sprite.width > cPosX) {
                        // if the object is lower than the bear position
                        if (cPosYPlushHeight <= objects[i].sprite.position.y) {
//                            graphics.lineStyle(1, 0xFF0000, 1);
//                            graphics.moveTo(objects[i].sprite.position.x, objects[i].sprite.position.y);
//                            graphics.lineTo(objects[i].sprite.position.x + objects[i].sprite.width, objects[i].sprite.position.y);

                            groundY = objects[i].sprite.position.y;
                        }
                    }
                }
            }
            return groundY;
        }

        this.buttonPressed = {
            isJumpButtonPressed: false,
            isCrouchButtonPressed: false,
            isAccelleratingButtonPressed: false,
            isDecelleratingButtonPressed: false
        }

        this.actions = {
            isJumping: false,
            isHoldingRope: false,
            isCrouching: false
        }

        this.physics = {
            GRAVITY: 4,
            velocityY: 0,
            velocityX: 4
        }


    }


    function _removeListeners() {

        if (!Sys.detectMobile()) {
            window.removeEventListener('keydown', Sys.stateObject.callback);

            H.off('tap panup pandown', function (e) {
                Sys.stateObject.callback(e);
            });
        } else {
            H.off('tap panup pandown', function (e) {
                Sys.stateObject.callback(e);
            });
        }
    }

    this.init = function () {

        game = new Game();
        Sys.framesPerSecond = 30;

        backgrounds = [];
        foregrounds = [];

        isMobile = Sys.detectMobile();


        if (Sys.level.iLevel === 1) {
            setTimeout(function () {
                Sys.stateObject.Tutorial(0);
            }, 3000);
        }

        for (var i = 0; i < 2; i++) {
            backgrounds[i] = new PIXI.Sprite(PIXI.Texture.fromFrame('landlayout_' + i + '.png'));
            backgrounds[i].position.x = i * backgrounds[i].width;
            foregrounds[i] = new PIXI.Sprite(PIXI.Texture.fromFrame('landlayout_front_' + i + '.png'));
            foregrounds[i].position.x = i * foregrounds[i].width;
            Sys.stage.addChild(backgrounds[i]);
        }

        finishPanelLose = new PIXI.Sprite(PIXI.Texture.fromFrame('gameOver.png'));

        for (var z = 0; z < 2; z++) {
            Sys.stage.addChild(foregrounds[z]);
        }
        backgroundWidth = backgrounds[1].width;

        /**
         * DEFINE CHARACTER MOVIECLIP
         * */
        var texture;
        for (var i = 0; i < 29; i++) {
            texture = PIXI.Texture.fromFrame(i + ".png");
            character.push(texture);
            texture = PIXI.Texture.fromFrame(i + '_crouch.png');
            character_crouch.push(texture);
        }
        game.groundY = Sys.sHeight - 50;
        game.character = new PIXI.MovieClip(character);
        game.character.position.x = (Sys.sWidth / 2) - (game.character.width * 2);
        game.character.position.y = (game.groundY - (game.character.height));
        /**
         * UI Elements
         */
        btnPause = new PIXI.Sprite(PIXI.Texture.fromFrame('topLeftBtn.png'));
        scoreText = new PIXI.Text('Score: 0', {font: '48px Coming Soon', fill: 'black'});


        btnContinue = new PIXI.Sprite(PIXI.Texture.fromFrame('continueBtn.png'));
        btnContinue.position.x = 320;
        btnContinue.position.y = 400;
        btnQuit = new PIXI.Sprite(PIXI.Texture.fromFrame('quitBtn.png'));
        btnRestart = new PIXI.Sprite(PIXI.Texture.fromFrame('restartBtn.png'));
        btnQuit.position.x = 235;
        btnQuit.position.y = 500;
        btnRestart.position.y = 500;
        btnRestart.position.x = 525;

        var btnLeftRun = new PIXI.Sprite(PIXI.Texture.fromFrame('leftRun.png'));
        btnLeftRun.position.x = 0;
        btnLeftRun.position.y = Sys.sHeight - btnLeftRun.height;
        btnLeftRun.alpha = 0.3;

        var btnRighttRun = new PIXI.Sprite(PIXI.Texture.fromFrame('rightRun.png'));
        btnRighttRun.alpha = 0.3;
        btnRighttRun.position.x = Sys.sWidth - btnRighttRun.width;
        btnRighttRun.position.y = Sys.sHeight - btnRighttRun.height;


        runningButton.push(btnLeftRun);
        runningButton.push(btnRighttRun);

        if (Sys.detectMobile()) {

            Sys.stage.addChild(btnLeftRun);
            Sys.stage.addChild(btnRighttRun);
        }


        /**
         * Define panel
         */
        panel = new PIXI.Sprite(PIXI.Texture.fromFrame('pause_menu.png'));
        blackBg = new PIXI.Sprite(PIXI.Texture.fromFrame('pauseEffect.png'));
        finishPanel = new PIXI.Sprite(PIXI.Texture.fromFrame('victory_menu.png'));
        finishPanel.position.x = Sys.sWidth;
        finishPanel.position.y = 0;
        /**
         * Add UID
         */
        scoreText.position.x = Sys.sWidth - (scoreText.width * 2.5);
        scoreText.position.y = scoreText.height / 2;
        Sys.stage.addChild(scoreText);
        /**
         * Add Character
         */
        Sys.stage.addChild(game.character);
        /**
         *
         */
        if (Sys.level.iLevel === 1) {
            intervalBg = new PIXI.Sprite(PIXI.Texture.fromFrame('background_tutorial.png'));
            intervalBg.position.x = Sys.sWidth / 2 - intervalBg.width / 2;
            intervalBg.position.y = Sys.sHeight / 2 - intervalBg.height / 2;
        }
        /**
         *
         */
        if (!Sys.detectMobile()) {
            window.addEventListener('keydown', Sys.stateObject.callback);

            H.on('tap panup pandown', function (e) {
                Sys.stateObject.callback(e);
            });
        } else {
            H.on('tap panup pandown', function (e) {
                Sys.stateObject.callback(e);
            });
        }
        btnContinue.interactive = true;
        btnQuit.interactive = true;
        btnRestart.interactive = true;
        btnPause.interactive = true;

        btnContinue.click = btnContinue.tap = function () {
            _pauseGame();
        }
        btnQuit.click = btnQuit.tap = function () {
            Sys.stateObject.destroy();
            //TODO = play a little closing animation.
            Sys.stateObject.end();
        }
        btnRestart.click = btnRestart.tap = function () {
            _restart();
        }


        btnPause.click = btnPause.tap = function () {
            if (Sys.level.iLevel != 1) {
                _pauseGame();
            }
        }

        game.init(game);
        Sys.sLoaded = true;
        Sys.gameLoop();
        Sys.stage.addChild(graphics);
        Sys.stage.addChild(btnPause);

        var intro = new Howl({
            urls: [dirAud + 'music/theMountain_intro.mp3', dirAud + 'music/theMountain_intro.ogg'],
            autoplay: true,
            loop: false,
            volume: (Sys.iMusic * 0.1),
            onend: function () {
                Sys.audio.returnAudio('gameLoop', 'music').loop(true);
                Sys.audio.returnAudio('gameLoop', 'music').volume(Sys.iMusic * 0.1);
                Sys.audio.returnAudio('gameLoop', 'music').play();
                intro.unload();
            }
        });
        intro.play();

        introDecoration = new PIXI.Sprite(PIXI.Texture.fromFrame('introDecoration.png'));


        introDecoration.position.y = Sys.sHeight / 2 - introDecoration.height / 2;
        introDecoration.position.x = Sys.sWidth / 2 - introDecoration.width / 2;


        introText = new PIXI.Text(Sys.level.sWorld.replace('_', ' ') + " - Level " + Sys.level.iLevel, {
            font: '50px Coming Soon',
            fill: '#fff'
        });

        introText.position.y = Sys.sHeight / 2 - introText.height / 2.5;
        introText.position.x = Sys.sWidth / 2 - introText.width / 2;


        Sys.stage.addChild(introDecoration);
        Sys.stage.addChild(introText);

        introInterval = setTimeout(function () {
            Sys.stage.removeChild(introText);
            Sys.stage.removeChild(introDecoration);
            clearInterval(introInterval);
        }, 2000);


    }

    /**
     * Show tutorial slides and stop the game loop waiting for the player to act
     * @param timeout
     * @param currentTime
     * @private
     */
    this.Tutorial = function (currenttime) {
        Sys.sLoaded = false;


        audioSprite.play("mmh");
        var currentTime = currenttime;
        var arrayElementToShow = [
            {img: 'tutorial_jumpUp.gif', text: 'Swipe up to jump.\n\nTap here to continue.', textD: 'Press spacebar \nto jump.\n\nClick here to continue.', timeout: 5500},
            {img: 'tutorial_getDown.gif', text: 'Swipe down\nto jump off\nthe platforms.\n\nTap here to continue.', textD: 'Press down key\nto jump off\nthe platforms.\n\nClick here to continue.', timeout: 15000},
            {img: 'tutorial_dangers.gif', text: 'Be careful!\nDangers are on\nyour way!\nThey will make you\n lose points.\nTry to avoid them!\nTap here to continue.', textD: 'Be careful!\nDangers are on\nyour way!\nThey will make you\n lose points.\nTry to avoid them!\nClick here to continue.', timeout: 10000},
            {img: 'tutorial_runFaster.gif', text: 'Running faster will\nget you a better score.\nTap fast right-left\nto run faster and\n JUMP HIGHER!\nTap here to continue.', text: 'Running faster will\nget you a better score.\nPress right-left key repeatedly\nto run faster and\n JUMP HIGHER!\nClick here to continue.', timeout: 10000},
            {img: 'tutorial_bonus.gif', text: 'Try to collect\nthe leafs and\nnuts to achieve\na greater score.\nTap here to continue.', timeout: 8000}
        ];

        var txt = arrayElementToShow[currentTime].text;

        if (!Sys.detectMobile()) {
            txt = arrayElementToShow[currentTime].textD;
        }

        tutorialText = new PIXI.Text(txt, {
            font: '24px Coming Soon',
            fill: '#EDEDED'
        });

        tutorialText.position.x = intervalBg.position.x + 45;
        tutorialText.position.y = intervalBg.position.y + 80;

        intervalTutorialSprite = new PIXI.Sprite(PIXI.Texture.fromFrame(arrayElementToShow[currentTime].img));
        intervalBg.interactive = true;

        intervalTutorialSprite.position.x = Sys.sWidth / 2 + 40;
        intervalTutorialSprite.position.y = intervalBg.position.y + 20;

        Sys.stage.addChild(blackBg);
        Sys.stage.addChild(intervalBg);
        Sys.stage.addChild(intervalTutorialSprite);
        Sys.stage.addChild(tutorialText);
        game.character.stop();

        intervalBg.tap = intervalBg.click = function () {
            Sys.stage.removeChild(intervalBg);
            Sys.stage.removeChild(intervalTutorialSprite);
            Sys.stage.removeChild(blackBg);
            Sys.stage.removeChild(tutorialText);
            Sys.sLoaded = true;
            game.character.play();
            // Recursive: spawn the new tutorial box in n seconds
            if (currentTime < arrayElementToShow.length - 1) {
                intervalTutorial = setTimeout(function () {
                        Sys.stateObject.Tutorial(currentTime + 1);
                    },
                    arrayElementToShow[currentTime].timeout);
            }
        }


    }


    /**
     * Restart the game
     * @private
     */
    function _restart() {
        Sys.sLoaded = false;
        Sys.setState('game');
        _removeListeners();
        dirRes = 'res/' + Sys.sWidth + '/cinema/';
        var loaderBg = new PIXI.Sprite(PIXI.Texture.fromImage(dirRes + 'loader/loader.gif'));
        Sys.stateObject.load(loaderBg);
        Sys.sLoaded = true;
    }

    function _pauseGame(bEnd) {
        Sys.audio.returnAudio('buttonPress', 'fx').play();

        if (bEnd === undefined) bEnd = false;

        if (Sys.sLoaded === true && bEnd == false) {


            Sys.sLoaded = false;
            Sys.stage.addChild(blackBg);
            Sys.stage.addChild(panel);
            Sys.stage.addChild(btnContinue);
            Sys.stage.addChild(btnRestart);
            Sys.stage.addChild(btnQuit);
            game.character.stop();
            Sys.audio.returnAudio('gameLoop', 'music').stop();


        } else {
            if (bEnd != true) {
                Sys.sLoaded = true;
            }
            try {
                Sys.stage.removeChild(btnContinueEnd);
                Sys.stage.removeChild(score_txt);
                // Force everything to play

                Sys.audio.returnAudio("gameLoop", 'music').stop();
                Sys.audio.returnAudio("gameLoop", 'music').loop(false);
                Sys.audio.returnAudio("gameLoop", 'music').pos(0);
                Sys.audio.returnAudio("mainTheme", 'music').loop(true);
                Sys.audio.returnAudio("mainTheme", 'music').pos(0);
                Sys.audio.returnAudio("mainTheme", 'music').play();
            } catch (e) {
                console.log("Trying to remove unexistent object");
            }


            Sys.stage.removeChild(graphics);
            Sys.stage.removeChild(panel);
            Sys.stage.removeChild(blackBg);
            Sys.stage.removeChild(btnQuit);
            Sys.stage.removeChild(btnContinue);
            Sys.stage.removeChild(btnRestart);
        }


    }

    function _positionText() {
        text.position.x = (Sys.sWidth / 2) - (text.width / 2);
        text.position.y = Sys.sHeight - text.height * 2;
    }

    function _calculateScore() {
        var totalScore = 0;

        // add up the score given by the objects
        totalScore += score;
        // add up the number of collectables
        totalScore += collectables * 10;

        // if all the collectables were collected then BONUS!
        if (collectables == totalCollectables) {
            totalScore += 1000;
        }


        //Each second of difference rises the score by 10 points
        var timeDifference = (potentialTime * Sys.framesPerSecond) - timeInFrames;
        timeDifference = timeDifference / Sys.framesPerSecond;
        timeDifference = parseInt(timeDifference);


        totalScore += (timeDifference * 10);

        return totalScore;
    }


    /**
     * Game Over methods
     * @private
     */
    function _gameOver() {
        /**
         * Game is over
         */
        if (bEnding === false) {
            clearInterval(intervalTutorial);
            try {
                Sys.stage.removeChild(btnPause);
                Sys.audio.returnAudio('gameLoop', 'music').stop(0);
                Sys.audio.returnAudio('gameLoop', 'music').pos(0);
            } catch (e) {
                console.log('Error');
            }

            var end = timeInFrames / Sys.framesPerSecond;


            // Calculate the time
            timeEnd = end;

            // Number of seconds to be displayed
            var minutes = parseInt(timeEnd / 60);
            var seconds = (timeEnd % 60).toFixed(2);


            window.removeEventListener('keydown', Sys.stateObject.callback);
            Sys.stage.addChild(blackBg);
            Sys.stage.addChild(finishPanel);
            bEnding = true;

            _calculateScore();

            if (_calculateScore() >= scoreToReach) {
                audioSprite.play('victory');
            } else {
                audioSprite.play('aw');
            }

            specialCollectable_txt = new PIXI.Text(totalCollectables.toString(), {
                font: '50px Coming Soon',
                fill: '#052a23'
            });
            time_txt = new PIXI.Text(( minutes + ":" + seconds), {
                font: '50px Coming Soon',
                fill: '#052a23'
            });
            score_txt = new PIXI.Text(_calculateScore().toString(), {font: '50px Coming Soon', fill: '#052a23'});


            // TODO make them relative to the screen size
            specialCollectable_txt.position.x = 375;
            specialCollectable_txt.position.y = 345;

            time_txt.position.x = 373;
            time_txt.position.y = 505;

            score_txt.position.x = 590;
            score_txt.position.y = 430;

            btnContinueEnd = new PIXI.Sprite(PIXI.Texture.fromFrame('continueBtn.png'));
            btnContinueEnd.position.x = 525;
            btnContinueEnd.position.y = 550;
            btnContinueEnd.interactive = true;
            btnContinueEnd.click = btnContinueEnd.tap = function () {
                bEndingScore = true;
            }

            try {
                Sys.stage.swapChildren(graphics, finishPanel);
                Sys.stage.swapChildren(finishPanel, blackBg);

            } catch (e) {
                console.log('could not swap children.')
            }

            var sound = new Howl({
                urls: [dirAud + 'fx/winning.mp3', dirAud + 'fx/winning.ogg'],
                volume: (Sys.iFxEffects * 0.1),
                buffer: true
            }).play();

            Sys.stage.addChild(time_txt);
            Sys.stage.addChild(specialCollectable_txt);
            Sys.stage.addChild(score_txt);
            Sys.stage.addChild(btnContinueEnd);

            if (_calculateScore() < scoreToReach) {
                finishPanelLose.position.x = 150;
                finishPanelLose.position.y = 5;
                Sys.stage.addChild(finishPanelLose);
            }

        } else {
            /** Game is ending, now perform final animation **/
            if (finishPanel.position.x > 0) {
                finishPanel.position.x -= 20;
                game.character.position.x += 20;
            } else {


                if (bEndingScore) {
                    var finalScore = parseInt(score_txt.text);


                    var newScore = false;
                    if (Sys.loadState('Level_' + Sys.level.iLevel) == undefined || finalScore > parseInt(Sys.loadState('Level_' + Sys.level.iLevel))) {
                        newScore = true;
                        Sys.saveState('Level_' + Sys.level.iLevel, finalScore);
                    }


                    graphics.clear();
                    graphics.beginFill(0xe6e3b8);
                    graphics.drawRect(200, 334, 565, 225);


                    btnRestart.position.x = 625;
                    btnRestart.position.y = 490;


                    if (finalScore >= scoreToReach) {
                        btnContinueEnd.click = btnContinueEnd.tap = function () {
                            Sys.level.iLevel = _unlockNewLevel();
                            _restart();
                        }
                    } else {
                        btnContinueEnd.click = btnContinueEnd.tap = function () {
                            _restart();
                        }

                    }


                    btnRestart.click = btnRestart.tap = function () {
                        Sys.stage.removeChild(finishPanel);
                        _restart();

                    }

                    btnQuit.click = btnQuit.tap = function () {

                        Sys.stage.removeChild(finishPanel);
                        Sys.stateObject.destroy();
                        //TODO = play a little closing animation.
                        Sys.audio.returnAudio('mainTheme', 'music').loop(true);
                        Sys.audio.returnAudio('mainTheme', 'music').play();
                        Sys.stateObject.end();
                    }

                    btnQuit.position.x = 180;
                    btnQuit.position.y = 560;


                    /**
                     *  TODO
                     *  Use the return to create score messages
                     */
                    var showResults = function (e) {
                        var objResult = Sys.level.process(e);
                        var text = '';
                        text = "You are in position #" + objResult.rank + "!";
                        /**
                         * TODO Add to the scores
                         */
//                        for(var i = 0 ; i < objResult.players_above.length; i ++){
//                            text += objResult.players_above[i].username + '/n';
//                        }
//
//                        text += 'lorenzo31 /n';
//
//                        for(var i = 0 ; i < objResult.players_below.length; i ++){
//                            text += objResult.players_above[i].username + '/n';
//                        }
                        score_txt.setText(text);
                        score_txt.position.x = Sys.sWidth / 4.5;
                    }

//                    var callback1 = function (e) {
//                        request = Sys.scores.makeRequest('lore.verri@gmail.com', 'lorenzo31', 'get', 1, 'normal', 350, null);
//                        Sys.scores.get(request, showResults);
//                    }

                    // TODO complete the score
//                    var request = Sys.scores.makeRequest('lore.verri@gmail.com', 'lorenzo31', 'update', 1, 'normal', 350, null);
//                    Sys.scores.get(request, callback1);

                    var newScoreText = newScore ? "New Score!\n" : "";

                    var compliments = ['Great!', 'Compliments!', 'Oh yeah!', 'Bravo!'];
                    var text = newScoreText + compliments[Math.floor(Math.random() * compliments.length)] + " \nYour score is " + finalScore + " / " + scoreToReach
                        + ".\nClick continue to go to \nthe next level!";


                    if (parseInt(score_txt.text) < scoreToReach) {
                        var ouchComments = ['Ouch!', 'Fudge!', 'Gah!'];
                        compliments = ouchComments;
                        text = newScoreText + compliments[Math.floor(Math.random() * compliments.length)] + " \nYour score is " + finalScore + " / " + scoreToReach
                            + ".\nClick continue to replay \nthe level!";
                    }


                    score_txt.setText(text);
                    score_txt.position.x = Sys.sWidth / 4.5;
                    score_txt.position.y = Sys.sHeight / 2 - score_txt.height / 8 + 50;
                    score_txt.setStyle({font: '36px Coming Soon'});
                    /**
                     *  TODO
                     *  Display them here
                     */
                    Sys.stage.removeChild(time_txt);
                    Sys.stage.removeChild(specialCollectable_txt);

                    Sys.stage.addChild(btnQuit);
                    Sys.stage.addChild(btnRestart);


                    bEndingScore = false;
                }
                return;
            }
        }
    }

    /**
     * Unlocks a new level if the player is playing the latest unlocked level
     * @private
     */
    function _unlockNewLevel() {
        //TODO -> add unlock level functionality
        var gameData = Sys.level.process(Sys.ajax.ajaxSync('', 'res/levels/levels.json')).worlds;
        var playerData = Sys.level.process(Sys.level.loadGame()).worlds;

        console.log(Sys.level.loadGame());

        var worldCurrentlyBeingPlayed = Sys.level.sWorld;
        var levelUnlocked;

        for (w in gameData) {
            if (w.toString() == worldCurrentlyBeingPlayed && Sys.level.iLevel < parseInt(gameData[w])) {
                if (playerData[w] == Sys.level.iLevel) {
                    playerData[w]++;
                    levelUnlocked = playerData[w];
                }
                console.log(w.toString() + " - new unlocked level is " + w)

                console.log(w.toString() + " - " + playerData[w]);
            } else if (w.toString() == worldCurrentlyBeingPlayed && Sys.level.iLevel == parseInt(gameData[w])) {
                // TODO - INCREASE NEXT WORLD LEVEL. YET, no levels will be out for now.
            }
        }

        Sys.level.saveGame(playerData.The_Mountain, playerData.The_Little_Hill, playerData.The_Seaside);

        if (levelUnlocked == undefined) {
            levelUnlocked = Sys.level.iLevel + 1;
        }

        return levelUnlocked;
        //Sys.level.saveGame(1, 0, 0);
    }


    /**
     * Loads all the graphics, audio and levels.
     */
    this.load = function (loaderSprite) {
        clearTimeout(Sys.animationFrame);
        if (loaderStates[loaderState] === 'graphics') {
            Sys.sLoaded = false;
            dirRes = 'res/' + Sys.sWidth + '/game/';
            loaderBg = loaderSprite;
            text = new PIXI.Text('Loading the graphics', {font: '50px Coming Soon', fill: 'black'});
            var dirResSprite = dirRes + 'SpriteSheet/';
            _positionText();

            var name = Sys.level.sWorld;
            if (Sys.level.iLevel > 6) {
                name += "_afternoon";
            }
            assetsToLoader = [ (dirResSprite + 'levels/' + name + '.json'),
                (dirResSprite + 'character/character.json'),
                (dirResSprite + 'character/character_crouching.json'),
                (dirResSprite + 'worlds/' + Sys.level.sWorld + '.json'),
                (dirResSprite + 'common/common.json')];

            if (Sys.level.iLevel === 1) {
                assetsToLoader.push((dirResSprite + 'tutorial/tutorial.json'));
            }

            loader = new PIXI.AssetLoader(assetsToLoader);


            Sys.stage.addChild(loaderBg);
            Sys.stage.addChild(text);
            loaderState++;

            Sys.renderer.render(Sys.stage);
            loader.onComplete = Sys.stateObject.load;
            loader.load();

        } else if (loaderStates[loaderState] === 'audio') {

            loader.removeAllListeners();

            audioSprite = new Howl({
                urls: ['audio/fx/gameSprite.ogg', 'audio/fx/gameSprite.mp3'],
                sprite: {
                    enemyBzz: [0, 2130],
                    munching: [2130, 1216],
                    stressed: [3346, 490],
                    aw: [3970, 736],
                    hit01: [4740, 178],
                    hit02: [4918, 280],
                    downTrail: [5253, 123],
                    jump01: [5665, 245],
                    jump02: [6000, 100],
                    victory: [6091, 1547],
                    careful: [7821, 421],
                    down: [8434, 223],
                    mmh: [8728, 702]
                },
                volume: Sys.iFxEffects,
                onload: function () {
                    Sys.stateObject.load()
                }
            });
            loaderState++;
            text.setText('Loading the music!');
            _positionText();
            Sys.renderer.render(Sys.stage);
        } else if (loaderStates[loaderState] === 'level') {
            var xmlhttp = new XMLHttpRequest();

            // Selects the right level

            xmlhttp.open('POST', 'res/levels/' + Sys.level.getDifficulty() + '/' + Sys.level.sWorld + '/' + Sys.level.iLevel + '.json', true);
            xmlhttp.send();
            loaderState++;
            text.setText('Loading the level stuff!');
            _positionText();
            Sys.renderer.render(Sys.stage);
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState === 4 && (xmlhttp.status === 200 || xmlhttp.status === 0)) {
                    levelObject = Sys.level.process(xmlhttp.responseText);
                    Sys.stateObject.load();
                }
            }
        } else if (loaderStates[loaderState] === 'end') {
            Sys.stage.removeChild(text);
            Sys.stage.removeChild(loaderBg);
            Sys.stateObject.init();
        }
    }

    //TODO remove this if not useful
    var vY = 0;
    /**
     * Update method
     */
    this.update = function () {
        if (Sys.sLoaded === true) {
            game.update();
            for (var i = 0; i < 2; i++) {

                if ((backgrounds[i].position.x + backgrounds[i].width) <= 0) {
                    var offset = backgrounds[i].position.x + backgrounds[i].width;
                    backgrounds[i].position.x = backgroundWidth + offset;
                }

                if ((foregrounds[i].position.x + foregrounds[i].width) <= 0) {
                    var offset = foregrounds[i].position.x + foregrounds[i].width;
                    foregrounds[i].position.x = backgroundWidth + offset;
                }

                backgrounds[i].position.x -= (game.physics.velocityX / 4) << 1;
                foregrounds[i].position.x -= (game.physics.velocityX) << 1;
            }
        }

    }

    /**
     * Pauses the game, and removes all the elements
     */
    this.end = function () {
        Sys.sLoaded = false;
        _removeListeners();
        Sys.resumeState();
        Sys.sLoaded = true;
    }


    /**
     * Callback for gestures and touchscreens
     * @param ev
     */
    this.callback = function (ev) {


        if (ev.type === "tap") {
            if (ev.center.y > Sys.sHeight / 2 && ev.center.x < Sys.sWidth / 2) {
                if (game.buttonPressed.isDecelleratingButtonPressed === false) {
                    runningButton[0].alpha = 0.9;
                    game.buttonPressed.isDecelleratingButtonPressed = true;
                }
            } else if (ev.center.y > Sys.sHeight / 2 && ev.center.x > Sys.sWidth / 2) {
                if (game.buttonPressed.isAccelleratingButtonPressed === false) {
                    runningButton[1].alpha = 0.9;
                    game.buttonPressed.isAccelleratingButtonPressed = true;
                }
            }
        }

        if (ev.type === "panup") {
            if (game.actions.isJumping === false) {
                game.buttonPressed.isJumpButtonPressed = true;
                return;
            }
        }
        if (ev.type === "pandown") {
            if (game.actions.isCrouching === false) {
                game.buttonPressed.isCrouchButtonPressed = true;
                return;
            }
        }
        //Jump
        if (ev.keyCode === 32) {
            if (game.actions.isJumping === false) {
                game.buttonPressed.isJumpButtonPressed = true;
            }
        } else if (ev.keyCode === 39)
        // Accellerating
        {
            if (game.buttonPressed.isAccelleratingButtonPressed === false) {
                game.buttonPressed.isAccelleratingButtonPressed = true;
            }
        } else if (ev.keyCode === 37) {
            if (game.buttonPressed.isDecelleratingButtonPressed === false) {
                game.buttonPressed.isDecelleratingButtonPressed = true;
            }
            // Crouching
        } else if (ev.keyCode === 40) {
            if (game.actions.isCrouching === false) {
                game.buttonPressed.isCrouchButtonPressed = true;
            }
            // Pause
        } else if (ev.keyCode === 80) {
            if (Sys.level.iLevel != 1) {
                _pauseGame();
            }
        }
    }

    /**
     * Collects all the sprites and removes them from the stage
     */
    this.destroy = function () {
        var i;
        try {
            clearInterval(intervalTutorial);
            clearInterval(introInterval);
            Sys.stage.removeChild(introText);
            Sys.stage.removeChild(introDecoration);
            Sys.stage.removeChild(finishPanelLose);
            Sys.stage.removeChild(finishPanel);
        } catch (e) {

        }
        _removeListeners();
        Sys.audio.returnAudio("gameLoop", 'music').stop();
        Sys.audio.returnAudio("gameLoop", 'music').loop(false);
        Sys.audio.returnAudio("gameLoop", 'music').pos(0);

//        Sys.audio.returnAudio('gameLoop', 'music').stop();
//        Sys.audio.returnAudio('mainTheme', 'music').loop(true);
//        Sys.audio.returnAudio('mainTheme', 'music').play();

        for (i = 0; i < runningButton.length; i++) {
            Sys.stage.removeChild(runningButton[i]);
        }

        for (i = 0; i < backgrounds.length; i++) {
            Sys.stage.removeChild(backgrounds[i]);
            Sys.stage.removeChild(foregrounds[i])
        }
        for (i = 0; i < game.objects.length; i++) {
            Sys.stage.removeChild(game.objects[i].sprite);
        }
        try {
            _pauseGame(true);
        } catch (e) {
            console.log('ok...');
        }
        Sys.stage.removeChild(game.character);
        Sys.stage.removeChild(scoreText);
        Sys.stage.removeChild(btnPause);


        audioSprite.unload();

        if (!Sys.detectMobile()) {
            window.removeEventListener('keydown', Sys.stateObject.callback);
        } else {
            H.on('swipeleft swiperight tap', function (e) {
                Sys.stateObject.callback(e);
            });
        }


    }


}


function ExtrasState() {

    var assetsToLoader;
    var loader;

    var background;
    var objects = [];
    var controls = [];
    var title;
    var init_animation = true;
    var end_animation = false;


    var currentOption = 1;


    this.update = function () {
        if (Sys.sLoaded) {
            /**
             * Move in animation
             */
            if (init_animation) {
                if (background.position.x > 0) {
                    background.position.x -= 60
                } else {
                    init_animation = false;
                }
            }
            /**
             * Move out animation
             */
            if (end_animation) {
                if (background.position.x < Sys.sWidth) {
                    background.position.x += 60;
                } else {
                    end_animation = false;
                }
            }
        }
    }


    this.load = function () {
        Sys.sLoaded = false;
        dirRes = 'res/' + Sys.sWidth + '/menu/';
        assetsToLoader = [(dirRes + 'SpriteSheet/ExtraSpriteSheet.json')];
        loader = new PIXI.AssetLoader(assetsToLoader);
        loader.onComplete = Sys.stateObject.init;
        loader.load();
    }

    this.init = function () {
        var texture = PIXI.Texture.fromFrame('extras_bg.png');
        var posX = Sys.sWidth;
        background = new PIXI.Sprite(texture);
        background.position.x = posX;
        background.position.y = 0;
        Sys.stage.addChild(background);


        var objButtons = [
            {sprite: 'cinema_btn.png', x: 100, y: 100},
            {sprite: 'credits_btn.png', x: 100, y: 100},
            {sprite: 'scores_btn.png', x: 100, y: 100}
        ];

        posX = 0;
        var sprite;
        for (var i = 0; i < objButtons.length; i++) {
            sprite = new PIXI.Sprite(PIXI.Texture.fromFrame(objButtons[i].sprite));
            sprite.position.y = (Sys.sHeight / 3) + posX;
            sprite.position.x = Sys.sWidth / 3 + posX;
            posX += 100;
            sprite.interactive = true;
            switch (objButtons[i].sprite) {
                case "cinema_btn.png":
                    sprite.click = sprite.tap = function () {
                        Sys.audio.returnAudio('buttonPress', 'fx').play();
                        Sys.stateObject.end('extras_cinema');
                    }
                    break;
                case "credits_btn.png":
                    sprite.click = sprite.tap = function () {
                        Sys.audio.returnAudio('buttonPress', 'fx').play();
                        Sys.stateObject.end('extras_about');
                    }
                    break;
                case "scores_btn.png":
                    sprite.click = sprite.tap = function () {
                        Sys.audio.returnAudio('buttonPress', 'fx').play();
                        Sys.stateObject.end('extras_scores');
                    }
                    break;
            }
            Sys.stage.addChild(sprite);
            objects.push(sprite);
        }

        // CONTROLS
        const arrows = ['arrow_back.png'];

        for (var i = 0; i < arrows.length; i++) {
            var sprite = new PIXI.Sprite(PIXI.Texture.fromFrame(arrows[i]));
            sprite.interactive = true;
            switch (arrows[i]) {
                case 'arrow_back.png':
                    sprite.position.x = 0 + (sprite.width / 2);
                    sprite.position.y = sprite.height / 2;
                    sprite.click = sprite.tap = function () {
                        Sys.audio.returnAudio('buttonPress', 'fx').play();
                        Sys.stateObject.end();
                    }
                    break;
            }
            Sys.stage.addChild(sprite);
            controls.push(sprite);
        }


//        TODO continue

        title = new PIXI.Sprite(PIXI.Texture.fromFrame('extras_title.png'));
        Sys.stage.addChild(title);
        title.position.x = Sys.sWidth / 2 - title.width / 2;
        title.position.y = title.height / 1.8;
        title.rotation = -0.05;

        currentOption = 1;
        Sys.sLoaded = true;

    }

    this.destroy = function () {
        Sys.stage.removeChild(background);
    }

    this.callback = function (evt) {
        Sys.audio.returnAudio('buttonPress', 'fx').play();
    }

    this.end = function (goToState) {
        for (var i = 0; i < controls.length; i++) {
            Sys.stage.removeChild(controls[i]);
        }

        for (var i = 0; i < objects.length; i++) {
            Sys.stage.removeChild(objects[i]);
        }

        Sys.stage.removeChild(title);

        end_animation = true;
        setTimeout(function () {
            if (goToState == null) {
                Sys.stateObject.destroy();
                Sys.resumeState();
            } else {

                Sys.setState(goToState);
                Sys.stateObject.load();
            }


        }, (animationLength * (currentOption + 1)));
    }
}


function ExtraScoresState() {

    var background;
    var objects = [];
    var controls = [];
    var scores = [];
    var title;
    var subTitle;
    var init_animation = true;
    var end_animation = false;

    var worlds = ['The Mountain', 'The Little Hill', 'The Seaside'];
    var backgroundBtn;
    var currentOption = 1;


    this.update = function () {
        if (Sys.sLoaded) {
            /**
             * Move in animation
             */
            if (init_animation) {
                if (background.position.x > 0) {
                    background.position.x -= 60
                } else {
                    init_animation = false;
                }
            }
            /**
             * Move out animation
             */
            if (end_animation) {
                if (background.position.x < Sys.sWidth) {
                    background.position.x += 60;
                } else {
                    end_animation = false;
                }
            }
        }
    }


    this.load = function () {
        Sys.sLoaded = false;
        dirRes = 'res/' + Sys.sWidth + '/menu/';
//        assetsToLoader = [(dirRes + 'SpriteSheet/ExtraSpriteSheet.json')];
//        loader = new PIXI.AssetLoader(assetsToLoader);
//        loader.onComplete = Sys.stateObject.init;
//        loader.load();
        Sys.stateObject.init();
    }

    this.init = function () {
        var texture = PIXI.Texture.fromFrame('options_bg.gif');
        var posY = Sys.sWidth;
        background = new PIXI.Sprite(texture);
        background.position.x = posY;
        background.position.y = 0;
        Sys.stage.addChild(background);

        var x = 20;
        var y = 200;

        backgroundBtn = [];

        for (var i = 0; i < worlds.length; i++) {

            backgroundBtn.push(new PIXI.Sprite(PIXI.Texture.fromFrame('base_btn.png')));
            backgroundBtn[i].position.x = x;
            backgroundBtn[i].position.y = y;

            worlds[i] = new PIXI.Text(worlds[i], {font: '32px Coming Soon', fill: '#fff'});
            worlds[i].position.x = x + 50;
            worlds[i].position.y = y + 25;


            Sys.stage.addChild(backgroundBtn[i]);
            Sys.stage.addChild(worlds[i]);


            x += 270;
        }

        var x = 70;
        var y = 300;

        var t;
        for (var i = 1; i < 13; i++) {
            var score = Sys.loadState('Level_' + i);
            if (score != null) {
                t = new PIXI.Text('Level ' + i + ": " + score, {font: '24px Coming Soon', fill: '#000'});
            } else {
                t = new PIXI.Text('Level ' + i + ": No score yet.", {font: '24px Coming Soon', fill: '#000'});
            }
            t.position.y = y;
            t.position.x = x;
            scores.push(t);
            y += 25;
        }

        for (var i = 0; i < scores.length; i++) {
            Sys.stage.addChild(scores[i]);
        }


        // CONTROLS
        const arrows = ['arrow_back.png'];

        for (var i = 0; i < arrows.length; i++) {
            var sprite = new PIXI.Sprite(PIXI.Texture.fromFrame(arrows[i]));
            sprite.interactive = true;
            switch (arrows[i]) {
                case 'arrow_back.png':
                    sprite.position.x = 0 + (sprite.width / 2);
                    sprite.position.y = sprite.height / 2;
                    sprite.click = sprite.tap = function () {
                        Sys.audio.returnAudio('buttonPress', 'fx').play();
                        Sys.stateObject.end('extras');
                    }
                    break;
            }
            Sys.stage.addChild(sprite);
            controls.push(sprite);
        }


//        TODO continue

        var subTitleBg = new PIXI.Sprite(PIXI.Texture.fromFrame('base_btn.png'));
        subTitleBg.position.x = Sys.sWidth / 2 - subTitleBg.width / 10;
        subTitleBg.position.y = (subTitleBg.height);
        subTitleBg.rotation = +0.05;
        Sys.stage.addChild(subTitleBg);


        title = new PIXI.Sprite(PIXI.Texture.fromFrame('extras_title.png'));
        Sys.stage.addChild(title);
        title.position.x = Sys.sWidth / 2 - title.width / 2;
        title.position.y = title.height / 1.8;
        title.rotation = -0.05;


        subTitle = new PIXI.Text('- Scores', {font: '32px Coming Soon', fill: '#e2e3af'});
        subTitle.position.x = Sys.sWidth / 2 + subTitle.width / 2;
        subTitle.position.y = title.position.y + subTitle.height * 1.5;
        Sys.stage.addChild(subTitle);


        currentOption = 1;
        Sys.sLoaded = true;

        objects.push(subTitle);
        objects.push(subTitleBg);
    }

    this.destroy = function () {
        Sys.stage.removeChild(background);
    }

    this.callback = function (evt) {
        Sys.audio.returnAudio('buttonPress', 'fx').play();
    }

    this.end = function (goToState) {
        for (var i = 0; i < controls.length; i++) {
            Sys.stage.removeChild(controls[i]);
        }

        for (var i = 0; i < backgroundBtn.length; i++) {
            Sys.stage.removeChild(backgroundBtn[i]);
            Sys.stage.removeChild(worlds[i]);
        }

        for (var i = 0; i < scores.length; i++) {
            Sys.stage.removeChild(scores[i]);
        }

        for (var i = 0; i < objects.length; i++) {
            Sys.stage.removeChild(objects[i]);
        }

        Sys.stage.removeChild(title);

        end_animation = true;
        setTimeout(function () {
            if (goToState == null) {
                Sys.stateObject.destroy();
                Sys.resumeState();
            } else {
                Sys.setState(goToState);
                Sys.stateObject.load();
            }


        }, (animationLength * (currentOption + 1)));
    }
}

function ExtraCreditsState() {
    var background;
    var objects = [];
    var controls = [];
    var title;
    var subTitle;
    var init_animation = true;
    var end_animation = false;


    var currentOption = 1;


    this.update = function () {
        if (Sys.sLoaded) {
            /**
             * Move in animation
             */
            if (init_animation) {
                if (background.position.x > 0) {
                    background.position.x -= 60
                } else {
                    init_animation = false;
                }
            }
            /**
             * Move out animation
             */
            if (end_animation) {
                if (background.position.x < Sys.sWidth) {
                    background.position.x += 60;
                } else {
                    end_animation = false;
                }
            }
        }
    }


    this.load = function () {
        Sys.sLoaded = false;
        dirRes = 'res/' + Sys.sWidth + '/menu/';
        Sys.stateObject.init();
    }

    this.init = function () {
        var texture = PIXI.Texture.fromFrame('options_bg.gif');
        var posY = Sys.sWidth;
        background = new PIXI.Sprite(texture);
        background.position.x = posY;
        background.position.y = 0;
        Sys.stage.addChild(background);


        var objButtons = [
            {sprite: 'Development', x: 100, y: 100},
            {sprite: 'Artworks', x: 100, y: 100},
            {sprite: 'Music', x: 100, y: 100},
            {sprite: 'Sounds Effect', x: 100, y: 100},


            {sprite: 'Lorenzo Verri', x: 400, y: 100},
            {sprite: 'Martina Tonello', x: 400, y: 300},
            {sprite: 'Lucandrea Baraldi', x: 400, y: 500},
            {sprite: 'Michal Wirazka', x: 400, y: 600}
        ];

        posY = 0;
        var sprite;
        for (var i = 0; i < objButtons.length; i++) {
            sprite = new PIXI.Text(objButtons[i].sprite, {font: '32px Coming Soon', fill: 'black'});
            sprite.position.y = (i > 3) ? objects[i - 4].position.y : (Sys.sHeight / 3) + posY;
            sprite.position.x = objButtons[i].x;//Sys.sWidth / 4;
            posY += 100;


            sprite.interactive = true;
            switch (objButtons[i].sprite) {
                case "Lorenzo Verri":
                    sprite.tap = sprite.click = function () {
                        var win = window.open('https://nl.linkedin.com/pub/lorenzo-verri/35/547/348', '_blank');
                        win.focus();
                    }
                    break;
                case "Martina Tonello":
                    sprite.tap = sprite.click = function () {
                        var win = window.open('http://martinut.tumblr.com/', '_blank');
                        win.focus();
                    }
                    break;
                case "Lucandrea Baraldi":
                    sprite.tap = sprite.click = function () {
                        var win = window.open('http://lucandreabaraldi.com/', '_blank');
                        win.focus();
                    }
                    break;
                case "Michal Wira":
                    sprite.tap = sprite.click = function () {
                        var win = window.open('vimeo.com/michalwiraszka', '_blank');
                        win.focus();
                    }
                    break;
            }
            Sys.stage.addChild(sprite);
            objects.push(sprite);
        }

        // CONTROLS
        const arrows = ['arrow_back.png'];

        for (var i = 0; i < arrows.length; i++) {
            var sprite = new PIXI.Sprite(PIXI.Texture.fromFrame(arrows[i]));
            sprite.interactive = true;
            switch (arrows[i]) {
                case 'arrow_back.png':
                    sprite.position.x = 0 + (sprite.width / 2);
                    sprite.position.y = sprite.height / 2;
                    sprite.click = sprite.tap = function () {
                        Sys.audio.returnAudio('buttonPress', 'fx').play();
                        Sys.stateObject.end('extras');
                    }
                    break;
            }
            Sys.stage.addChild(sprite);
            controls.push(sprite);
        }


//        TODO continue

        var subTitleBg = new PIXI.Sprite(PIXI.Texture.fromFrame('base_btn.png'));
        subTitleBg.position.x = Sys.sWidth / 2 - subTitleBg.width / 10;
        subTitleBg.position.y = (subTitleBg.height);
        subTitleBg.rotation = +0.05;
        Sys.stage.addChild(subTitleBg);


        title = new PIXI.Sprite(PIXI.Texture.fromFrame('extras_title.png'));
        Sys.stage.addChild(title);
        title.position.x = Sys.sWidth / 2 - title.width / 2;
        title.position.y = title.height / 1.8;
        title.rotation = -0.05;


        subTitle = new PIXI.Text('- Credits', {font: '32px Coming Soon', fill: '#e2e3af'});
        subTitle.position.x = Sys.sWidth / 2 + subTitle.width / 2;
        subTitle.position.y = title.position.y + subTitle.height * 1.5;
        Sys.stage.addChild(subTitle);


        currentOption = 1;
        Sys.sLoaded = true;

        objects.push(subTitle);
        objects.push(subTitleBg);
    }

    this.destroy = function () {
        Sys.stage.removeChild(background);
    }

    this.callback = function (evt) {
        Sys.audio.returnAudio('buttonPress', 'fx').play();
    }

    this.end = function (goToState) {
        for (var i = 0; i < controls.length; i++) {
            Sys.stage.removeChild(controls[i]);
        }

        for (var i = 0; i < objects.length; i++) {
            Sys.stage.removeChild(objects[i]);
        }

        Sys.stage.removeChild(title);

        end_animation = true;
        setTimeout(function () {
            if (goToState == null) {
                Sys.stateObject.destroy();
                Sys.resumeState();
            } else {
                Sys.setState(goToState);
                Sys.stateObject.load();
            }


        }, (animationLength * (currentOption + 1)));
    }
}

function ExtraCinemaState() {
    var background;
    var objects = [];
    var controls = [];
    var texts = [];
    var title;
    var subTitle;
    var init_animation = true;
    var end_animation = false;


    var currentOption = 1;


    this.update = function () {
        if (Sys.sLoaded) {
            /**
             * Move in animation
             */
            if (init_animation) {
                if (background.position.x > 0) {
                    background.position.x -= 60
                } else {
                    init_animation = false;
                }
            }
            /**
             * Move out animation
             */
            if (end_animation) {
                if (background.position.x < Sys.sWidth) {
                    background.position.x += 60;
                } else {
                    end_animation = false;
                }
            }
        }
    }


    this.load = function () {
        Sys.sLoaded = false;
        dirRes = 'res/' + Sys.sWidth + '/menu/';
//        assetsToLoader = [(dirRes + 'SpriteSheet/ExtraSpriteSheet.json')];
//        loader = new PIXI.AssetLoader(assetsToLoader);
//        loader.onComplete = Sys.stateObject.init;
//        loader.load();
        Sys.stateObject.init();
    }

    this.init = function () {
        var texture = PIXI.Texture.fromFrame('options_bg.gif');
        var posY = Sys.sWidth;
        texts = ['Intro - Play'];
        background = new PIXI.Sprite(texture);
        background.position.x = posY;
        background.position.y = 0;
        Sys.stage.addChild(background);


        var objButtons = [
            {sprite: 'base_btn.png', x: 200, y: 200}
        ];

        posY = 0;
        var sprite;
        for (var i = 0; i < objButtons.length; i++) {
            var sprite = new PIXI.Sprite(PIXI.Texture.fromFrame(objButtons[i].sprite));
            texts[i] = new PIXI.Text(texts[i], {font: '32px Coming Soon', fill: '#fff'});

            sprite.position.y = (Sys.sHeight / 3) + posY;
            sprite.position.x = Sys.sWidth / 2 - sprite.width / 2;

            texts[i].position.x = sprite.position.x + sprite.width / 4;
            texts[i].position.y = sprite.position.y - texts[i].height / 2 + 40;
            posY += 100;
            sprite.interactive = true;
            sprite.click = objButtons[i].tap = function () {
                var win = window.open('https://www.youtube.com/watch?v=CA_LzQPPzlg', '_blank');
                win.focus();
            };

            Sys.stage.addChild(sprite);
            Sys.stage.addChild(texts[i]);
            objects.push(sprite);
        }

        // CONTROLS
        const arrows = ['arrow_back.png'];

        for (var i = 0; i < arrows.length; i++) {
            var sprite = new PIXI.Sprite(PIXI.Texture.fromFrame(arrows[i]));
            sprite.interactive = true;
            switch (arrows[i]) {
                case 'arrow_back.png':
                    sprite.position.x = 0 + (sprite.width / 2);
                    sprite.position.y = sprite.height / 2;
                    sprite.click = sprite.tap = function () {
                        Sys.audio.returnAudio('buttonPress', 'fx').play();
                        Sys.stateObject.end('extras');
                    }
                    break;
            }
            Sys.stage.addChild(sprite);
            controls.push(sprite);
        }


//        TODO continue

        var subTitleBg = new PIXI.Sprite(PIXI.Texture.fromFrame('base_btn.png'));
        subTitleBg.position.x = Sys.sWidth / 2 - subTitleBg.width / 10;
        subTitleBg.position.y = (subTitleBg.height);
        subTitleBg.rotation = +0.05;
        Sys.stage.addChild(subTitleBg);


        title = new PIXI.Sprite(PIXI.Texture.fromFrame('extras_title.png'));
        Sys.stage.addChild(title);
        title.position.x = Sys.sWidth / 2 - title.width / 2;
        title.position.y = title.height / 1.8;
        title.rotation = -0.05;


        subTitle = new PIXI.Text('- Cinematics', {font: '32px Coming Soon', fill: '#e2e3af'});
        subTitle.position.x = Sys.sWidth / 2 + subTitle.width / 2;
        subTitle.position.y = title.position.y + subTitle.height * 1.5;
        Sys.stage.addChild(subTitle);


        currentOption = 1;
        Sys.sLoaded = true;

        objects.push(subTitle);
        objects.push(subTitleBg);
    }

    this.destroy = function () {
        Sys.stage.removeChild(background);


    }

    this.callback = function (evt) {
        Sys.audio.returnAudio('buttonPress', 'fx').play();
    }

    this.end = function (goToState) {
        for (var i = 0; i < controls.length; i++) {
            Sys.stage.removeChild(controls[i]);
        }
        for (var i = 0; i < texts.length; i++) {
            Sys.stage.removeChild(texts[i]);
        }

        for (var i = 0; i < objects.length; i++) {
            Sys.stage.removeChild(objects[i]);
        }

        Sys.stage.removeChild(title);

        end_animation = true;
        setTimeout(function () {
            if (goToState == null) {
                Sys.stateObject.destroy();
                Sys.resumeState();
            } else {
                Sys.setState(goToState);
                Sys.stateObject.load();
            }


        }, (animationLength * (currentOption + 1)));
    }
}


function CinemaState() {

    var video;
    var text;
    var loaderBg;
    var graphics;


    var texture;
    var moveSprite;

    this.update = function () {

    }

    this.returnVideo = function () {
        return video;
    }

    this.end = function () {
        Sys.setState('game');
        Sys.stateObject.load(loaderBg);
    }

    this.load = function (sVideo) {
//        Sys.sLoaded = false;
        dirRes = 'res/' + Sys.sWidth + '/cinema/';
        loaderBg = new PIXI.Sprite(PIXI.Texture.fromImage(dirRes + 'loader/loader.gif'));

        if (Sys.level.iLevel === 1 && Sys.level.sWorld === "The_Mountain") {


            video = document.createElement('video');
            video.setAttribute('preload', 'preload');
            video.setAttribute('width', Sys.sWidth.toString());
            video.setAttribute('height', Sys.sHeight.toString());
            video.setAttribute('autoplay', 'autoplay');
//            video.setAttribute('controls', 'controls');
            video.setAttribute('style', 'position:absolute;top:0px;left:-10px;display:none;');

            var extensions = ['mp4', 'ogg'];


            for (var i = 0; i < extensions.length; i++) {
                var source = document.createElement('source');
                source.setAttribute('type', 'video/' + extensions[i]);
                source.setAttribute('src', dirRes + sVideo + '.' + extensions[i]);
                video.appendChild(source);

            }
            document.body.appendChild(video);
            Sys.stateObject.init(Sys.detectMobile());
            return video;

        } else {
            Sys.stateObject.end();
        }
    }


    this.showText = function () {

        Sys.stage.addChild(text);
    }


    this.init = function (isMobile) {


        Sys.sLoaded = true;
        Sys.audio.returnAudio('mainTheme', 'music').fadeOut();

        if (Sys.level.iLevel === 1 && Sys.level.sWorld === "The_Mountain") {

            if (isMobile || navigator.userAgent.match(/Firefox/i) == "Firefox") {

                video.setAttribute('style', 'position:absolute;top:0px;display:inline;');
                text = document.createElement('p');
                text.innerHTML = "Press to continue";
                video.setAttribute('width', window.innerWidth.toString());
                video.setAttribute('height', window.innerHeight.toString());
                text.setAttribute('style', 'position:absolute; z-index:1000; color:white; top:80%; left:40%; font-size:48px;');
                text.setAttribute('id', 'skipVideoText')

                text.addEventListener('click', function () {
                    video.play();

                    setTimeout(function () {
                        var old_element = text;
                        var textNode = text.cloneNode(true);
                        try {
                            old_element.parentNode.replaceChild(textNode, old_element);
                        }
                        catch (e) {

                        }
                        textNode.innerHTML = "Tap to skip";
                        textNode.setAttribute('style', 'position:absolute; z-index:1000; color:white; top:80%; left:38%; font-size:48px;');
                        Sys.fullScreen(canvas);
                        textNode.addEventListener('click', Sys.stateObject.callback);
                    }, 1000)

                });
                document.body.appendChild(text);
            } else {


                text = new PIXI.Text('Tap here to continue', {font: '32px Coming Soon', fill: 'black'});
                text.position.x = Sys.sWidth / 2 - text.width / 2;
                text.position.y = Sys.sHeight - text.height * 3;
                texture = PIXI.VideoTexture.textureFromVideo(video);
                texture.baseTexture.source.volume = Sys.iFxEffects * 0.1;
                // create a new Sprite using the texture
                moveSprite = new PIXI.Sprite(texture);
                // move the sprite to the center of the screen
                moveSprite.position.x = Sys.sWidth / 2;
                moveSprite.position.y = Sys.sHeight / 2;
                moveSprite.anchor.x = 0.5;
                moveSprite.anchor.y = 0.5;
                moveSprite.width = 960;
                moveSprite.height = 640;
                text.interactive = true;
                Sys.stage.addChild(moveSprite);

                setTimeout(
                    Sys.stateObject.showText
                    , 3000);


                text.tap = text.click = function (e) {
                    Sys.stateObject.callback(e);
                }

            }
//

        } else {
            // Skip straight to the game.
//            clearTimeout(Sys.animationFrame);
            Sys.stateObject.callback();
        }
    }

    this.callback = function (ev) {
        Sys.stateObject.end();
    }

    this.destroy = function () {

        if (Sys.detectMobile() || (navigator.userAgent.match(/Firefox/i) == "Firefox")) {
            try {
                document.body.removeChild(video);
                document.body.removeChild(document.getElementById('skipVideoText'));
                video.src = '';
                text.removeEventListener('click', Sys.stateObject.callback);
            } catch (e) {
                console.log("problems removing the video" + e);
            }
        }

        if (moveSprite != null) {
            Sys.stage.removeChild(moveSprite);
            // unloads the video
            Sys.stage.removeChild(text);
            moveSprite.texture.baseTexture.source.src = "";
            // destroy base texture
            moveSprite.texture.destroy(true);
        }

    }


}


var Sys;

Sys = {
    animationFrame: {},
    /**
     * Screen width and height
     */
    sWidth: 0,
    sHeight: 0,
    /**
     * TODO - encapsulate these into an object
     * State is the name of the state being currently played
     * State object holds the instance of the played state
     */
    state: "",
    stateObject: 0,
    pausedStateObject: 0,
    /**
     * Stage and Renderer are required by PIXI
     */
    stage: '',
    renderer: '',
    /**
     * State loaded boolean
     */
    sLoaded: false,

    /**
     * Settings
     */
    iMusic: 3,
    iFxEffects: 5,
    iDifficulty: 2,

    /**
     * Game Speed
     */
    framesPerSecond: 40,


    level: {
        iLevel: 1,
        sWorld: "",
        process: function (jsonObj) {
            var json = JSON.parse(jsonObj);
            return json;
        },
        getDifficulty: function () {
            if (Sys.iDifficulty === 1) return 'medium';
            if (Sys.iDifficulty === 2) return 'medium';
            if (Sys.iDifficulty === 3) return 'medium';
            if (Sys.iDifficulty === 4) return 'medium';
            return '';
        },
        loadGame: function () {
            var gameState = Sys.loadState('SavedGame');
            if (gameState == null) {
                Sys.level.saveGame(1, 0, 0);
            }
            return Sys.loadState('SavedGame');
        },
        saveGame: function (iMountain, iTheLittleHill, iTheSeaside) {
            var gameToSave = '{"worlds": {"The_Mountain": ' + iMountain + ',"The_Little_Hill": ' + iTheLittleHill + ',"The_Seaside": ' + iTheSeaside + '}}';
            Sys.saveState('SavedGame', gameToSave);
        }

    },


    checkSystem: function () {
        if (typeof(Storage) == "undefined") {
            throw new Sys.mrgrizzException('The local storage is not supported by your browser. Please update your system');
        }
    },


    init: function () {
        Sys.checkSystem();
        Sys.detectScreenSize();
        //creates a stage which is interactive
        Sys.stage = new PIXI.Stage(0xF5F1D4, true);

        Sys.renderer = new PIXI.autoDetectRenderer(Sys.sWidth, Sys.sHeight);

        document.getElementById('container').appendChild(Sys.renderer.view);
        Sys.renderer.view.style.position = "absolute";
        Sys.renderer.view.style.top = "0px";
        Sys.renderer.view.style.left = "0px";
        H = new Hammer(document.getElementById('container'));
        //H.get('swipe').set({ direction: Hammer.DIRECTION_ALL });
        dirAud = 'audio/';
        Sys.loadSettings();
        Sys.audio.init();

        /**
         * TO DEBUG THE GAME
         */
        if (Sys.state != 'game') {
            Sys.stateObject.init();
            Sys.gameLoop();
        } else {
            Sys.stateObject.load();
        }


    },

    loadSettings: function () {
        var value = Sys.loadState('FX Effects');
        if (value != null) {
            Sys.iFxEffects = value;
        } else {
            Sys.iFxEffects = 5;
            Sys.saveState('FX Effects', 5);
        }
        value = Sys.loadState('Difficulty');
        if (value != null) {
            Sys.iDifficulty = parseInt(value);
        } else {
            Sys.iDifficulty = 2;
            Sys.saveState('Difficulty', 2);
        }
        value = Sys.loadState('Music');
        if (value != null) {
            Sys.iMusic = value;
        } else {
            Sys.iMusic = 3;
            Sys.saveState('Music', 2)
        }
    },

    pauseState: function () {
        Sys.stateObject.pause();
        Sys.pausedStateObject = Sys.stateObject;
    },

    resumeState: function () {
        Sys.stateObject = Sys.pausedStateObject;
        Sys.stateObject.resume();
    },

    setState: function (state) {
        Sys.state = state;
        if (Sys.stateObject != 0) {
            Sys.stateObject.destroy();
        }
        switch (Sys.state) {
            case "intro":
                Sys.stateObject = new IntroState();
                break;
            case "menu":
                Sys.stateObject = new MenuState();
                break;
            case "options":
                Sys.stateObject = new OptionsState();
                break;
            case "level_selection":
                Sys.stateObject = new LevelSelectionState();
                break;
            case "cinema":
                Sys.stateObject = new CinemaState();
                break;
            case "game":
                Sys.stateObject = new GameState();
                break;
            case "extras_about":
                Sys.stateObject = new ExtraCreditsState();
                break;
            case "extras_cinema":
                Sys.stateObject = new ExtraCinemaState();
                break;
            case "extras_scores":
                Sys.stateObject = new ExtraScoresState();
                break;
            case "extras":
                Sys.stateObject = new ExtrasState();
                break;
            default :
                throw new Sys.mrgrizzException("The state was not found");
                break;
        }

    },
    mrgrizzException: function (message) {
        this.message = message;
        this.name = "mrGrizzException";
    },
    gameLoop: function () {
        Sys.stateObject.update();
        Sys.animationFrame = setTimeout(Sys.gameLoop
            , 1000 / Sys.framesPerSecond);
        Sys.renderer.render(Sys.stage);

    },
    timeStamp: function () {
        return window.performance && window.performance.now ? window.performance.now() : Date.now();
    },
    detectScreenSize: function () {
        Sys.sWidth = 480;
        Sys.sHeight = 320;

        Sys.sWidth = 960;
        Sys.sHeight = 640;
//        if (window.innerWidth > 479) Sys.sWidth = 800;
//        Sys.sHeight = 480;
//        if (window.innerWidth > 959) Sys.sWidth = 960;
//        Sys.sHeight = 640;
    },
    checkOrientation: function () {
        var orientation = window.orientation;
        var container = document.getElementById('container');

        if (orientation == '0' && window.innerWidth < window.innerHeight) {
            //TODO show the background plus arrow
            document.getElementsByTagName('canvas')[0].style.display = 'none';
            var elem = document.createElement('div');
            elem.setAttribute('id', 'arrow');
            Sys.sLoaded = false;
            container.appendChild(elem);
        } else {
            Sys.sLoaded = true;
            document.getElementsByTagName('canvas')[0].style.display = 'inline'
            try {
                container.removeChild(document.getElementById('arrow'));
            } catch (type) {
                console.log('Type');
            }
        }
    },
    fullScreen: function (obj) {
        var elem = obj;
        if (!Sys.detectMobile()) return;
        if (elem.requestFullscreen) {
            elem.requestFullscreen();
        } else if (elem.msRequestFullscreen) {
            elem.msRequestFullscreen();
        } else if (elem.mozRequestFullScreen) {
            elem.mozRequestFullScreen();
        } else if (elem.webkitRequestFullscreen) {
            elem.webkitRequestFullscreen();
        }
    },
    exitFullScreen: function (obj) {
        if (document.exitFullscreen) {
            document.exitFullscreen();
        } else if (document.webkitExitFullscreen) {
            document.webkitExitFullscreen();
        } else if (document.mozCancelFullScreen) {
            document.mozCancelFullScreen();
        } else if (document.msExitFullscreen) {
            document.msExitFullscreen();
        }
    },
    scores: {
        xmlhttp: new XMLHttpRequest(),
        makeRequest: function (email, password, type, level, difficulty, score, nationality) {
            var userAgent = "mobile";
            userAgent = (Sys.detectMobile()) ? "mobile" : "desktop";
            if (nationality == null) {
                return 'email=' + email + '&password=' + password + '&type=' + type + '&data={"level":' + level + ',"userAgent":"' + userAgent + '","difficulty":"' + difficulty + '","score":' + score + '}';
            }
            return 'email=' + email + '&password=' + password + '&type=' + type + '&data={"level":' + level + ',"userAgent":"' + userAgent + '","difficulty":"' + difficulty + '","score":' + score + ',"nationality":"' + nationality + '"}';
        },
        get: function (params, callback) {
            Sys.scores.xmlhttp.open('POST', '../backend/init.php?');
            Sys.scores.xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            Sys.scores.xmlhttp.send(params);
            Sys.scores.callback(callback);
        },
        callback: function (callbackObj) {
            Sys.scores.xmlhttp.onreadystatechange = function (callback) {
                if (Sys.scores.xmlhttp.readyState === 4 && Sys.scores.xmlhttp.status === 200) {
                    callbackObj(Sys.scores.xmlhttp.responseText);
                }
            }
        }
    },
    ajax: {
        newRequest: function (params, address, callback) {
            Sys.scores.xmlhttp = new XMLHttpRequest();
            Sys.scores.xmlhttp.open('GET', address);
            Sys.scores.xmlhttp.send(params);
            Sys.scores.callback(callback);
        },
        ajaxSync: function (params, address) {
            Sys.scores.xmlhttp = new XMLHttpRequest();
            Sys.scores.xmlhttp.open('POST', address, false);
            Sys.scores.xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            Sys.scores.xmlhttp.send(params);
            return Sys.scores.xmlhttp.responseText;
        }
    },
    detectMobile: function () {
        if (navigator.userAgent.match(/Android/i)
            || navigator.userAgent.match(/webOS/i)
            || navigator.userAgent.match(/iPhone/i)
            || navigator.userAgent.match(/iPad/i)
            || navigator.userAgent.match(/iPod/i)
            || navigator.userAgent.match(/BlackBerry/i)
            || navigator.userAgent.match(/Windows Phone/i)
            ) {
            return true;
        }
        else {
            return false;
        }
    },
    saveState: function (sKey, sValue) {
        localStorage.setItem(sKey, sValue)

    },
    loadState: function (sKey) {
        return localStorage.getItem(sKey);
    },

    audio: {
        aMusic: [],
        fxSounds: [],
        init: function () {


            var mainTheme = new Howl({
                urls: [dirAud + 'music/menu_loop.mp3', dirAud + 'music/menu_loop.ogg'],
                volume: (Sys.iMusic * 0.1),
                loop: true
            });
            var buttonSound = new Howl({
                urls: [ dirAud + 'fx/button_press.mp3', dirAud + 'fx/button_press.ogg'],
                volume: (Sys.iFxEffects * 0.1),
                buffer: true
            });

            var gameLoop = new Howl({
                urls: [ dirAud + 'music/theMountain_loop.mp3', dirAud + 'music/theMountain_loop.ogg'],
                volume: (Sys.iMusic * 0.1),
                loop: true
            });


            mainTheme = {name: 'mainTheme', sound: mainTheme};
            Sys.audio.aMusic.push(mainTheme);


            gameLoop = {name: 'gameLoop', sound: gameLoop};
            Sys.audio.aMusic.push(gameLoop);


            buttonSound = {name: 'buttonPress', sound: buttonSound};
            Sys.audio.fxSounds.push(buttonSound);
        },
        returnAudio: function (sAudio, type) {
            if (type === 'music') {
                for (var i = 0; i < Sys.audio.aMusic.length; i++) {
                    var sound = Sys.audio.aMusic[i];
                    if (sAudio === sound.name) {
                        return sound.sound;
                    }
                }
            } else {
                for (var i = 0; i < Sys.audio.fxSounds.length; i++) {
                    var sound = Sys.audio.fxSounds[i];
                    if (sAudio === sound.name) {
                        return sound.sound;
                    }
                }
            }
        },


        setVolume: function (type) {
            if (type === 'music') {
                for (var i = 0; i < Sys.audio.aMusic.length; i++) {
                    //console.log('Music Volume' + Sys.iMusic * 0.1);
                    Sys.audio.aMusic[i].sound.volume((Sys.iMusic * 0.1));
                }
            } else {
                for (var i = 0; i < Sys.audio.fxSounds.length; i++) {
                    Sys.audio.fxSounds[i].sound.volume((Sys.iFxEffects * 0.1));
                }
            }
        }
    },


    WebService: {

        initDialog: function () {
            document.querySelector('#sender').addEventListener('click', function (e) {
                e.preventDefault();

                var email = document.querySelector('#email').value;
                var password = document.querySelector('#password').value;
                var test = Sys.WebService.testWebService(email, password);


                if (test == false) {

                    return false;
//                    TODO Display error
                } else if (test == "Error password") {
//                    Error password
                    return false;
                } else {
                    // Save it
                    Sys.WebService.setWebService(email, password);
                    return true;
                }

            });
            document.querySelector('#cancel').addEventListener('click', function (e) {
                e.preventDefault();
                Sys.WebService.hideDialog();
            });
        },

        /**
         * Saves the credentials in the js
         * storage
         * @param email
         * @param password
         */
        setWebService: function (email, password) {

        },

        /**
         * Display errors in either the user
         */
        displayError: function (textError) {
            document.querySelector('#accessPanelError').style.display = 'inline';
            document.querySelector('#accessPanelErrorText').innerHTML = textError;
        },
        /**
         * Test a web service and return a boolean
         * @param email
         * @param password
         */
        testWebService: function (email, password) {
            Sys.WebService.blockUnblockDialog(true);
            if (email != '' && password != '') {
                var requestString = 'type=test&email=' + email + '&password=' + password;
                if (Sys.ajax.ajaxSync(requestString, '../backend/init.php?').indexOf('True') != -1) {
                    Sys.WebService.blockUnblockDialog(false);
                    return true;
                } else {
                    Sys.WebService.blockUnblockDialog(true);
                    return "Error password";
                }
            } else {
                return false;
            }
        },

        showDialog: function () {
            document.querySelector('#accessPanel').style.display = 'inline';
        },

        hideDialog: function () {
            document.querySelector('#accessPanel').style.display = 'none';
        },

        blockUnblockDialog: function (bBlock) {
            document.querySelector('#sender').disabled = bBlock;
            document.querySelector('#cancel').disabled = bBlock;
        },

        registerValues: function (email, password, saveCredentials) {
            Sys.saveState('username', email);
            Sys.saveState('password', password);

            if (saveCredentials) {
                Sys.saveState('savedCredentials', 'true');
            }

        },

        loadValues: function () {
            var c = Sys.loadState('savedCredentials');
            if (c == 'true') {
                return true;
            }
            return false;
        }
    },


    Local: {
        worker: '',
        init: function () {
            Sys.Local.worker = new Worker("Threads/Install.js");
        }
    }

};


window.onload = function () {


    Sys.WebService.initDialog();
    Sys.WebService.showDialog();


    Sys.setState("intro");
    Sys.init();


    // Sys.checkOrientation();
//    Sys.checkOrientation();
    document.body.addEventListener('click', function (ev) {
        canvas = ev.target;
        canvas.style.width = "100%";
        Sys.fullScreen(document.body);
    });
    // TODO on mobiles it should work only if orientation is 90 or -90
    window.onorientationchange = Sys.checkOrientation;
}